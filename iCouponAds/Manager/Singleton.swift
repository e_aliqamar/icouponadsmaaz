//
//  Singleton.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import CoreData

private let singleton = Singleton()

class Singleton {
    
    var CurrentUser: UserObject? = UserManager.getUserObjectFromUserDefaults()
    
    class var sharedInstance: Singleton {
        return singleton
    }
}
