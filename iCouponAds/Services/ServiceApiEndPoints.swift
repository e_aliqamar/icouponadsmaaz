//
//  ServiceApiEndPoints.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class ServiceApiEndPoints: NSObject {
    
    static let baseUrl  = "https://olxseventyseven.com/farhan/icoupanAds"
    
    static let login = baseUrl + "/signin.json"
    static let register = baseUrl + "/register.json"
    static let forgotPassword = baseUrl + "/forgetpassword.json"
    static let editProfile = baseUrl + "/editProfile.json"
    
    static let getFlyers = baseUrl + "/getFlyers.json"
    static let getFavoriteFlyers = baseUrl + "/getFavouriteFlyers.json"
    static let favoriteFlyer = baseUrl + "/favouriteFlyer.json"
    static let unfavoriteFlyer = baseUrl + "/unFavouriteFlyer.json"
    static let getCoupons = baseUrl + "/get-coupans-by-type.json"
    static let getSingleCoupon = baseUrl + "/get-coupan.json"
    static let addToCartCoupon = baseUrl + "/add-to-cart.json"
    static let removeFromCartCoupon = baseUrl + "/remove-cart-item.json"
    static let getCartCoupon = baseUrl + "/get-items-from-cart.json"
    static let getClippings = baseUrl + "/flyerworking.html"
    static let getVideos = baseUrl + "/getCarousel.json"
    static let getHotDeals = baseUrl + "/getHotDeals.json"
    static let getInStoreDeals = baseUrl + "/getInStores.json"
    static let getSaveDeals = baseUrl + "/getSaveDeals.json"
    static let getCategories = baseUrl + "/getCategories.json"
    static let saveDeal = baseUrl + "/saveDeal.json"
    static let unsaveDeal = baseUrl + "/unSaveDeal.json"
    
    static let search = baseUrl + "/search.json"
    
}
