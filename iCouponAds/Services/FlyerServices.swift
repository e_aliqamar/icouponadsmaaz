//
//  FlyerServices.swift
//  iCouponAds
//
//  Created by Ali Qamar on 04/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import SwiftyJSON

class FlyerServices {
    
    //MARK:- Get Flyers Service Method
    static func GetFlyers(param: [String: Any],
                          completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getFlyers, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Get Favorite Flyers Service Method
    static func GetFavoriteFlyers(param: [String: Any],
                          completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getFavoriteFlyers, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Favorite Flyer Service Method
    static func FavoriteFlyer(param: [String: Any],
                                  completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.favoriteFlyer, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Unfavorite Flyer Service Method
    static func UnfavoriteFlyer(param: [String: Any],
                              completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.unfavoriteFlyer, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
