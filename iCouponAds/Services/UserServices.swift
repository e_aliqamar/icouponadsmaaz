//
//  UserServices.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserServices {
    
    //MARK:- Login Service Method
    static func Login(param: [String: Any],
                             completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.login, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Register Service Method
    static func Register(param: [String: Any],
                      completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.register, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Forgot Password Service Method
    static func ForgotPassword(param: [String: Any],
                         completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.forgotPassword, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Edit Profile Service Method
    static func EditProfile(param: [String: Any],
                               completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.editProfile, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
