//
//  HomeServices.swift
//  iCouponAds
//
//  Created by Ali Qamar on 05/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import SwiftyJSON

class HomeServices {
    
    //MARK:- Get Videos Service Method
    static func GetVideos(param: [String: Any],
                             completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getVideos, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Get Hot Deals Service Method
    static func GetHotDeals(param: [String: Any],
                          completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getHotDeals, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Get In Store Deals Service Method
    static func GetInStoreDeals(param: [String: Any],
                            completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getInStoreDeals, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Get Save Deals Service Method
    static func GetSaveDeals(param: [String: Any],
                                completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getSaveDeals, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Save Deals Service Method
    static func SaveDeal(param: [String: Any],
                             completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.saveDeal, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- UnSave Deals Service Method
    static func UnSaveDeal(param: [String: Any],
                         completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.unsaveDeal, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Get Categories Service Method
    static func GetCategories(param: [String: Any],
                             completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getCategories, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
