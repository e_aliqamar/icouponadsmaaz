//
//  CouponServices.swift
//  iCouponAds
//
//  Created by Ali Qamar on 04/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import SwiftyJSON

class CouponServices {
    
    //MARK:- Get Coupons Service Method
    static func GetCoupons(param: [String: Any],
                          completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getCoupons, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Get Single Coupons Service Method
    static func GetSingleCoupon(param: [String: Any],
                           completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getSingleCoupon, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Add To Cart Coupons Service Method
    static func AddToCart(param: [String: Any],
                                 completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.addToCartCoupon, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Get Cart Coupons Service Method
    static func GetCart(param: [String: Any],
                          completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getCartCoupon, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Remove From Cart Coupons Service Method
    static func RemoveFromCart(param: [String: Any],
                          completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.PostRequest(url: ServiceApiEndPoints.removeFromCartCoupon, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:- Search Coupons and Flyers Service Method
    static func Search(param: [String: Any],
                               completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.search, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
