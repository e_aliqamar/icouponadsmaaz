//
//  ClippingServices.swift
//  iCouponAds
//
//  Created by Ali Qamar on 05/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import SwiftyJSON

class ClippingServices {
    
    //MARK:- Get Clippings Service Method
    static func GetClippings(param: [String: Any],
                           completionHandler: @escaping (_ success: Bool, _ response: JSON?,_ error: Any?) -> Void) {
        
        Request.GetRequest(url: ServiceApiEndPoints.getClippings, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                return
            }
            
            if !(response?["status"].boolValue)! {
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
