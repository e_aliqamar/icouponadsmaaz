//
//  Alert.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import Foundation

class Alert: NSObject {
    
    static func showAlert(title: String, message: String){
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
            // do something after completation
        }
        alert.addAction(alertOkAction)
        AppDelegate.getInstatnce().window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
    
}
