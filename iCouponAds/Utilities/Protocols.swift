//
//  Protocols.swift
//  iCouponAds
//
//  Created by Ali Qamar on 05/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation

@objc protocol ButtonClickDelegate {
    @objc optional func click(indexPath: IndexPath)
    
    @objc optional func shareClick(indexPath: IndexPath)
    @objc optional func locationClick(indexPath: IndexPath)
    @objc optional func saveClick(indexPath: IndexPath)
    @objc optional func playClick(indexPath: IndexPath)
    @objc optional func detailClick(indexPath: IndexPath)
}

@objc protocol FindMeDelegate {
    @objc optional func findMe(zipCode: String)
}

@objc protocol CategoriesDelegate {
    @objc optional func next(indexes: [IndexPath])
    @objc optional func skip()
}
