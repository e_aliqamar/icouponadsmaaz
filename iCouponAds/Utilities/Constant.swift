//
//  Constant.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

let User_data_userDefault       = "User_data_userDefault"

let WINDOW_FRAME                = UIScreen.main.bounds
let SCREEN_SIZE                 = UIScreen.main.bounds.size
let WINDOW_WIDTH                = UIScreen.main.bounds.size.width
let WINDOW_HIEGHT               = UIScreen.main.bounds.size.height

let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

let USER_DEFAULTS               = UserDefaults.standard


let THEME_COLOR: UInt           = 0x00BCE9
let TITLE_LOGO                  = UIImage(named: "icoupon_logo")
