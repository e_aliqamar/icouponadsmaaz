//
//  Enums.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation

enum FLYERTYPE: String {
    case FEATURED
    case A_Z
    case LATEST
    case FAVORITE
}

enum COUPONTYPE: String {
    case FEATURED
    case DOLLAR_OFF
    case PERCENT_OFF
    case LATEST
}

enum SHOPPINGLISTTYPE: String {
    case SHOPPINGLIST
    case CLIPPINGLIST
}

enum HOMETYPE: String {
    case HOTDEALS
    case INSTORE
    case SAVEDEALS
}

enum SEARCHTYPE: String {
    case ALL
    case FLYER
    case COUPON
}
