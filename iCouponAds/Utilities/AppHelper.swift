//
//  AppHelper.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class AppHelper: NSObject {
    
    static func isValid(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
    
    static func isValid(password: String) -> Bool {
        return (password.count < 6 || password.count > 16) ? false : true
    }
    
    static func isValid(name: String) -> Bool {
        return (name.count < 2 || name.count > 35) ? false : true
    }
    
    static func isValid(zipCode: String) -> Bool {
        return (zipCode.count < 5 || zipCode.count > 11) ? false : true
    }
    
    static func formateDate(_ date: Date) -> String {
        //Thu, Jan 29, 2015
        return self.formateDate(date, dateFormat: "eee, MMM dd,  YYYY");
    }
    
    //IT IS RETURNING DATE IN ISO 8601 WITHOUT COMBINING TIME
    static func formateDateInISO(_ date: Date) -> String {
        //2015-12-15
        return self.formateDate(date, dateFormat: "YYYY-MM-dd");
    }
    
    static func formateDate(_ date: Date, dateFormat: String) -> String {
        let formatrer:DateFormatter = DateFormatter();
        formatrer.dateFormat = dateFormat;
        
        return formatrer.string(from: date);
    }
    
}
