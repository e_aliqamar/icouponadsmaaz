//
//  DetailViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 06/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class DetailViewController: UIViewController {

    class func instantiateFromStoryboard() -> DetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DetailViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgLogo: UIImageViewAligned!
    @IBOutlet weak var lblUrl: UILabel!
    @IBOutlet weak var lblDisount: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    
    // MARK: - Properties
    
    var detailHotDeals: HotDeals!
    var detailSaveDeals: SaveDeals!
    
    var isSave: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        backButton()
        
        self.imgLogo.alignLeft = true
        if detailHotDeals != nil {
            self.title = detailHotDeals.businessName!
            self.imgBanner.setImageFromUrl(urlStr: detailHotDeals.displayImage!)
            self.imgLogo.setImageFromUrl(urlStr: detailHotDeals.logoImage!)
            self.lblUrl.text = detailHotDeals.dealWebsite!
            self.txtDescription.text = detailHotDeals.dealDescription!
            
            if detailHotDeals.isSave == 1 {
                self.btnSave.setImage(UIImage(named: "deal_saved_icon"), for: .normal)
            } else {
                self.btnSave.setImage(UIImage(named: "save_icon"), for: .normal)
            }
            
        } else {
            self.title = detailSaveDeals.businessName!
            self.imgBanner.setImageFromUrl(urlStr: detailSaveDeals.displayImage!)
            self.imgLogo.setImageFromUrl(urlStr: detailSaveDeals.displayImage!)
            self.lblUrl.text = detailSaveDeals.dealWebsite!
            self.txtDescription.text = detailSaveDeals.dealDescription!
            
            self.btnSave.setImage(UIImage(named: "deal_saved_icon"), for: .normal)
            self.isSave = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func share(text: String) {
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop,UIActivityType.postToWeibo,UIActivityType.message,UIActivityType.mail,UIActivityType.print,UIActivityType.copyToPasteboard,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList, UIActivityType.postToFlickr, UIActivityType.openInIBooks,UIActivityType.postToVimeo,UIActivityType.postToTencentWeibo,UIActivityType.markupAsPDF]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: - IBAction
    
    @IBAction func locationAction(_ sender: UIButton) {
        if detailHotDeals != nil {

            let obj = self.detailHotDeals
            let vc = LocationViewController.instantiateFromStoryboard()
            vc.detailHotDeals = obj
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            
            let obj = self.detailSaveDeals
            let vc = LocationViewController.instantiateFromStoryboard()
            vc.detailSaveDeals = obj
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        if detailHotDeals != nil {
            if self.detailHotDeals.isSave == 1 {
                self.unSaveDeal(id: self.detailHotDeals.tDealId!, completionHandler: { (status) in
                    if status {
                        self.detailHotDeals.isSave = 0
                        self.btnSave.setImage(UIImage(named: "save_icon"), for: .normal)
                    }
                })
            } else {
                self.saveDeal(id: self.detailHotDeals.tDealId!, completionHandler: { (status) in
                    if status {
                        self.detailHotDeals.isSave = 1
                        self.btnSave.setImage(UIImage(named: "deal_saved_icon"), for: .normal)
                    }
                })
            }
        } else {
            if self.isSave {
                self.unSaveDeal(id: self.detailSaveDeals.tDealId!, completionHandler: { (status) in
                    if status {
                        self.isSave = false
                        self.btnSave.setImage(UIImage(named: "save_icon"), for: .normal)
                    }
                })
            } else {
                self.saveDeal(id: self.detailSaveDeals.tDealId!, completionHandler: { (status) in
                    if status {
                        self.isSave = true
                        self.btnSave.setImage(UIImage(named: "deal_saved_icon"), for: .normal)
                    }
                })
            }
            
        }
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        if detailHotDeals != nil {
            self.share(text: detailHotDeals.businessWebsite!)
        } else {
            self.share(text: detailSaveDeals.dealWebsite!)
        }
        
    }

}

//MARK:-  Service Methods

extension DetailViewController {
    
    func saveDeal(id: String, completionHandler: @escaping (_ status: Bool) -> Void) {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        HomeServices.SaveDeal(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!, "t_deal_id": id], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    completionHandler(false)
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                completionHandler(false)
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            completionHandler(true)
            
        })
    }
    
    func unSaveDeal(id: String, completionHandler: @escaping (_ status: Bool) -> Void) {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        HomeServices.UnSaveDeal(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!, "t_deal_id": id], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    completionHandler(false)
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                completionHandler(false)
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            completionHandler(true)
            
        })
    }
}
