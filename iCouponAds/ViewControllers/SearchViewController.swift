//
//  SearchViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 17/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class SearchViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SearchViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SearchViewController
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    
    var selectedType: SEARCHTYPE = .ALL
    let cellIdentifier = "FlyersFeaturedCollectionViewCell"
    let cellIdentifier2 = "CouponFeaturedCollectionViewCell"
    
    var flyersArray: [Flyer]? = [Flyer]()
    var couponsArray: [Coupon]? = [Coupon]()
    
    var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        //        self.title = "Bread"
        
        backButton()
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        let nib2 = UINib(nibName: cellIdentifier2, bundle: nil)
        
        self.collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        self.collectionView.register(nib2, forCellWithReuseIdentifier: cellIdentifier2)
        
        self.collectionView.alwaysBounceVertical = true
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(self.refreshStream), for: .valueChanged)
        
        collectionView!.addSubview(refresher)
        
        self.search()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func refreshStream() {
        
        print("refresh")
        
        self.flyersArray?.removeAll()
        self.couponsArray?.removeAll()
        self.collectionView.reloadData()
        
        self.search()
    }
    
    // MARK: - IBAction
    
    @IBAction func segmentedControlAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.selectedType = .ALL
            break
        case 1:
            self.selectedType = .FLYER
            break
        case 2:
            self.selectedType = .COUPON
            break
        default: break
        }
        self.collectionView.reloadData()
    }
}

//MARK:-  UICollectionViewDelegate, UICollectionViewDataSource

extension SearchViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedType == .ALL {
            return (self.flyersArray?.count)! + (self.couponsArray?.count)!
        } else if self.selectedType == .FLYER {
            return (self.flyersArray?.count)!
        }
        return (self.couponsArray?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.selectedType == .ALL {
            if (self.flyersArray?.count)! > indexPath.row {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! FlyersFeaturedCollectionViewCell
                
                let obj = self.flyersArray![indexPath.row]
                
                cell.imgBanner.setImageFromUrl(urlStr: obj.image!)
                cell.imgLogo.setImageFromUrl(urlStr: obj.businessLogo!)
                
                if obj.isNew == "1" {
                    cell.viewIsNew.isHidden  = false
                } else {
                    cell.viewIsNew.isHidden  = true
                }
                
                cell.lblTitle.text = obj.descriptionValue!
                cell.lblDaysLeft.text = "\((obj.expiryDate!.toDate().daysLeft())) left"
                
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier2, for: indexPath) as! CouponFeaturedCollectionViewCell
                
                let obj = self.couponsArray![indexPath.row - (self.flyersArray?.count)!]
                
                cell.imgBanner.setImageFromUrl(urlStr: obj.imglink!)
                cell.imgIcon.setImageFromUrl(urlStr: obj.businessLogo!)
                cell.lblPrice.text = "AED \(obj.currentAmount!)"
                cell.lblActualPrice.text = "AED \(obj.currentAmount!)"
                cell.lblDiscountPrice.text = "AED \(obj.totalAmount!)"
                cell.lblPercentOff.text = "AED \(obj.saveAmount!)"
                cell.lblDescription.text = obj.descriptionValue!
                cell.lblTitle.text = obj.coupan!
                cell.lblPercent.text = obj.save!
                
                return cell
            }
        } else if self.selectedType == .FLYER {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! FlyersFeaturedCollectionViewCell
            
            let obj = self.flyersArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.image!)
            cell.imgLogo.setImageFromUrl(urlStr: obj.businessLogo!)
            
            if obj.isNew == "1" {
                cell.viewIsNew.isHidden  = false
            } else {
                cell.viewIsNew.isHidden  = true
            }
            
            cell.lblTitle.text = obj.descriptionValue!
            cell.lblDaysLeft.text = "\((obj.expiryDate!.toDate().daysLeft())) left"
            
            return cell
            
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier2, for: indexPath) as! CouponFeaturedCollectionViewCell
        
        let obj = self.couponsArray![indexPath.row]
        
        cell.imgBanner.setImageFromUrl(urlStr: obj.imglink!)
        cell.imgIcon.setImageFromUrl(urlStr: obj.businessLogo!)
        cell.lblPrice.text = "AED \(obj.currentAmount!)"
        cell.lblActualPrice.text = "AED \(obj.currentAmount!)"
        cell.lblDiscountPrice.text = "AED \(obj.totalAmount!)"
        cell.lblPercentOff.text = "AED \(obj.saveAmount!)"
        cell.lblDescription.text = obj.descriptionValue!
        cell.lblTitle.text = obj.coupan!
        cell.lblPercent.text = obj.save!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.selectedType == .ALL {
            if (self.flyersArray?.count)! > indexPath.row {
                let numberOfItems: CGFloat = 1.0
                let padding: CGFloat = 0.0
                
                var collectionViewSize = collectionView.frame.size
                collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
                collectionViewSize.height = collectionViewSize.height / 2.5
                return collectionViewSize
            } else {
                let numberOfItems: CGFloat = 1.0
                let padding: CGFloat = 0.0
                
                var collectionViewSize = collectionView.frame.size
                collectionViewSize.width = (collectionViewSize.width / numberOfItems - padding)
                collectionViewSize.height = 198//collectionViewSize.height / 2.5
                return collectionViewSize
            }
        } else if self.selectedType == .FLYER {
            let numberOfItems: CGFloat = 1.0
            let padding: CGFloat = 0.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
            collectionViewSize.height = collectionViewSize.height / 2.5
            return collectionViewSize
        }
        
        let numberOfItems: CGFloat = 1.0
        let padding: CGFloat = 0.0
        
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = (collectionViewSize.width / numberOfItems - padding)
        collectionViewSize.height = 198//collectionViewSize.height / 2.5
        return collectionViewSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        
        if self.selectedType == .ALL {
            if (self.flyersArray?.count)! > indexPath.row {
//                let obj = self.flyersArray![indexPath.row]
            } else {
                let obj = self.couponsArray![indexPath.row - (self.flyersArray?.count)!]
                let vc = CouponDetailViewController.instantiateFromStoryboard()
                vc.coupon = obj
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if self.selectedType == .FLYER {
//            let obj = self.flyersArray![indexPath.row]
        }
        
        let obj = self.couponsArray![indexPath.row]
        let vc = CouponDetailViewController.instantiateFromStoryboard()
        vc.coupon = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:-  Service Methods

extension SearchViewController {
    
    func search() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        let parameters = ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!,
                          "t_search": self.title!]
        CouponServices.Search(param: parameters, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            self.refresher?.endRefreshing()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let couponsResponseArray = response?["data"]["coupan"].arrayValue
            //            print(couponsResponseArray!)
            for resultObj in couponsResponseArray! {
                let obj = Coupon(json: resultObj)
                self.couponsArray?.append(obj)
            }
            
            let flyersResponseArray = response?["data"]["flyer"].arrayValue
            //            print(flyersResponseArray!)
            for resultObj in flyersResponseArray! {
                let obj = Flyer(json: resultObj)
                self.flyersArray?.append(obj)
            }
            
            self.segmentedControl.setTitle("All (\((self.flyersArray?.count)! + (self.couponsArray?.count)!))", forSegmentAt: 0)
            self.segmentedControl.setTitle("Flyers (\((self.flyersArray?.count)!))", forSegmentAt: 1)
            self.segmentedControl.setTitle("Coupons (\((self.couponsArray?.count)!))", forSegmentAt: 2)
            
            
            self.collectionView.reloadData()
        })
    }
}

