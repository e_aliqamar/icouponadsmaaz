//
//  RootViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class RootViewController: SlideMenuController {
    
    class func instantiateFromStoryboard() -> RootViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RootViewController
    }
    
    override func awakeFromNib() {
//        super.awakeFromNib()
        // Initialization code
        
        if let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RootTabBarViewController") as? RootTabBarViewController {
            self.mainViewController = controller
        }
        let leftVc = RootNavigationViewController.instantiateFromStoryboard()
        leftVc.viewControllers = [SideMenuViewController.instantiateFromStoryboard()]
        self.leftViewController = leftVc
        
        let rightVc = RootNavigationViewController.instantiateFromStoryboard()
        rightVc.viewControllers = [RightSideMenuViewController.instantiateFromStoryboard()]
        self.rightViewController = rightVc
        
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.leftViewWidth = WINDOW_WIDTH
        SlideMenuOptions.rightViewWidth = WINDOW_WIDTH
        SlideMenuOptions.contentViewDrag = true
        SlideMenuOptions.panGesturesEnabled = false
        super.awakeFromNib()
    }

}
