//
//  SideMenuViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SideMenuViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuViewController
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    
    var items: [(name: String, image: UIImage)] = [(name: "All Flyers", image: UIImage(named: "categories_allflyer")!),
                                                  (name: "Automative", image: UIImage(named:"categories_automative")!),
                                                  (name: "Baby & Kids", image: UIImage(named:"categories_baby")!),
                                                  (name: "Electronic", image: UIImage(named:"categories_electronics")!),
                                                  (name: "Fashion", image: UIImage(named:"categories_fashion")!),
                                                  (name: "Groceries", image: UIImage(named:"categories_groceries")!),
                                                  (name: "Home", image: UIImage(named:"categories_home")!),
                                                  (name: "Office", image: UIImage(named:"categories_office")!),
                                                  (name: "Pets", image: UIImage(named:"categories_pets")!),
                                                  (name: "Pharmacy", image: UIImage(named:"categories_pharmacy")!),
                                                  (name: "Sports", image: UIImage(named:"categories_sports")!)]
    
    let cellIdentifier = "SideMenuTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Categories"
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        self.navigationController?.resetNavigationBar()
        cancelButton()
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
    }
    
    //MARK:-  Navigation Bar Button Code
    
    func cancelButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        rightBtn.setTitle("Cancel", for: .normal)
        rightBtn.addTarget(self, action: #selector(self.cancelBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func cancelBtnPressed() {
        closeLeft()
    }
    
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SideMenuTableViewCell
        
        cell.lblTitle.text = items[indexPath.row].name
        cell.imgIcon.image = items[indexPath.row].image
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        switch indexPath.row {
        case 0:
            break
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            break
        case 5:
            break
        case 6:
            break
        case 7:
            break
        case 8:
            break
        case 9:
            break
        default:
            break
        }
    }
}
