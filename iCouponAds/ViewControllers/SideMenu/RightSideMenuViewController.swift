//
//  RightSideMenuViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 03/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class RightSideMenuViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> RightSideMenuViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RightSideMenuViewController
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    
    var items: [String] = ["Profile", "Notifications", "My Loyalty Points", "Signout"]
    let cellIdentifier = "RightSideMenuTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Settings"
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        self.navigationController?.resetNavigationBar()
        cancelButton()
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:-  Navigation Bar Button Code
    
    func cancelButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setTitle("Cancel", for: .normal)
        leftBtn.addTarget(self, action: #selector(self.cancelBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func cancelBtnPressed() {
        closeRight()
    }
    
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension RightSideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: RightSideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! RightSideMenuTableViewCell
        
        cell.lblTitle.text = items[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        switch indexPath.row {
        case 0:
            let vc = ProfileViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = NotificationViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            break
        case 3:
            let alert = UIAlertController(title: "Signout", message: "Do you want to sign out?", preferredStyle: UIAlertControllerStyle.alert)
            
            let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                // do something after completation
                UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                UserDefaults.standard.synchronize()
                
                Singleton.sharedInstance.CurrentUser = nil
                
                let vc = RootNavigationViewController.instantiateFromStoryboard()
                vc.viewControllers = [LoginViewController.instantiateFromStoryboard()]
                UIWINDOW?.rootViewController = vc
            }
            let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (action) -> Void in
                // do something after completation
            }
            alert.addAction(alertYesAction)
            alert.addAction(alertNoAction)
            DispatchQueue.main.async {
                AppDelegate.getInstatnce().window?.rootViewController!.present(alert, animated: true, completion: nil)
            }
            
            
            break
        default:
            break
        }
    }
}


