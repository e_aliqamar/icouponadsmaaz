//
//  LocationViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 11/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> LocationViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LocationViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var imgLogo: UIImageViewAligned!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblTimings: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    // MARK: - Properties
    
    var detailHotDeals: HotDeals!
    var detailInStoreDeals: InStore!
    var detailSaveDeals: SaveDeals!
    
    @objc var marker: GMSMarker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        self.title = "Map"
        backButton()
        
        self.imgLogo.alignLeft = true
        
        if detailHotDeals != nil {
            self.imgLogo.setImageFromUrl(urlStr: detailHotDeals.logoImage!)
            self.lblTitle.text = detailHotDeals.businessName!
            self.lblDays.text = "\(detailHotDeals.businessOpenDay!) -  \(detailHotDeals.businessClosedDay!)"
            self.lblTimings.text = "\(detailHotDeals.businessOpenTime!) to  \(detailHotDeals.businessClosedTime!)"
            self.lblAddress.text = detailHotDeals.businessAddress!
            self.initPointer(title: detailHotDeals.businessName!, subtitle: detailHotDeals.dealName!, lat: Double(self.detailHotDeals.businessLat!)!, long: Double(self.detailHotDeals.businessLong!)!)
        } else if detailInStoreDeals != nil {
            self.imgLogo.setImageFromUrl(urlStr: detailInStoreDeals.logoImage!)
            self.lblTitle.text = detailInStoreDeals.businessName!
            self.lblDays.text = "\(detailInStoreDeals.businessOpenDay!) -  \(detailInStoreDeals.businessClosedDay!)"
            self.lblTimings.text = "\(detailInStoreDeals.businessOpenTime!) to  \(detailInStoreDeals.businessClosedTime!)"
            self.lblAddress.text = detailInStoreDeals.businessAddress!
            self.initPointer(title: detailInStoreDeals.businessName!, subtitle: detailInStoreDeals.businessName!, lat: Double(self.detailInStoreDeals.businessLat!)!, long: Double(self.detailInStoreDeals.businessLong!)!)
        } else {
            self.imgLogo.setImageFromUrl(urlStr: detailSaveDeals.displayImage!)
            self.lblTitle.text = detailSaveDeals.businessName!
            self.lblDays.text = "\(detailSaveDeals.businessOpenDay!) -  \(detailSaveDeals.businessClosedDay!)"
            self.lblTimings.text = "\(detailSaveDeals.businessOpenTime!) to  \(detailSaveDeals.businessClosedTime!)"
            self.lblAddress.text = detailSaveDeals.businessAddress!
            self.initPointer(title: detailSaveDeals.businessName!, subtitle: detailSaveDeals.dealName!, lat: Double(self.detailSaveDeals.businessLat!)!, long: Double(self.detailSaveDeals.businessLong!)!)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initPointer(title: String, subtitle: String, lat: Double, long: Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long), zoom: 12)
        self.mapView.camera = camera
        
        let location = CLLocationCoordinate2DMake(CLLocationDegrees(lat), CLLocationDegrees(long))
        marker = GMSMarker()
        marker.position = location
        marker.title = title
        marker.snippet = subtitle
        marker.icon = UIImage(named: "pin")
        marker.appearAnimation = .pop
        marker.map = mapView
        
        mapView.selectedMarker = marker
    }
}
