//
//  AddCategoryViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 12/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class AddCategoryViewController: UIViewController {

    class func instantiateFromStoryboard() -> AddCategoryViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AddCategoryViewController
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    
    let cellIdentifier = "AddCategoryTableViewCell"
    var numberOfRows = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        self.title = "Add Category"
        
        backButton()
        doneButton()
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func doneButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        rightBtn.setImage(UIImage(named: "checked"), for: .normal)
        rightBtn.addTarget(self, action: #selector(self.doneBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func doneBtnPressed() {
        
    }
    
    @IBAction func addCategoryAction(_ sender: UIButton) {
        self.numberOfRows = self.numberOfRows + 1
        let indexPath = IndexPath.init(row: self.numberOfRows - 1, section: 0)
        self.tableView.insertRows(at: [indexPath], with: .fade)
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension AddCategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: AddCategoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AddCategoryTableViewCell
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (row, indexPath) in
            
            if self.tableView.numberOfRows(inSection: 0) == 1 {
                return
            }
            
            self.numberOfRows = self.numberOfRows - 1
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        return [deleteAction]
    }
    
}
