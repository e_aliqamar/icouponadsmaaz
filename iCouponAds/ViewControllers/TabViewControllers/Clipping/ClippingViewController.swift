//
//  ClippingViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class ClippingViewController: UIViewController {

    class func instantiateFromStoryboard() -> ClippingViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ClippingViewController
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    let cellIdentifier = "ClippingTableViewCell"
    var clippingsArray: [Clipping]? = [Clipping]()
    
    var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Clippings"
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        self.navigationController?.resetNavigationBar()
        
        backButton()
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
        
        self.tableView.alwaysBounceVertical = true
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(self.refreshStream), for: .valueChanged)
        
        tableView!.addSubview(refresher)
        
        self.getClippings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func refreshStream() {
        
        print("refresh")
        
        self.clippingsArray?.removeAll()
        self.tableView?.reloadData()
        
        self.getClippings()
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension ClippingViewController: UITableViewDelegate, UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.clippingsArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ClippingTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ClippingTableViewCell
        
        let obj = self.clippingsArray![indexPath.row]
        
        cell.clipping = obj
        cell.collectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
    }
}


//MARK:-  Service Methods

extension ClippingViewController {
    
    func getClippings() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        ClippingServices.GetClippings(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            self.refresher?.endRefreshing()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let clippingResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in clippingResponseArray! {
                let obj = Clipping(json: resultObj)
                self.clippingsArray?.append(obj)
            }
            
            self.tableView.reloadData()
            
        })
    }
}
