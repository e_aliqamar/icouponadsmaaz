//
//  CouponViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class CouponViewController: UIViewController {

    class func instantiateFromStoryboard() -> CouponViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CouponViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var txtSearch: UITextField!
    
    // MARK: - Properties

    var selectedType: COUPONTYPE = .FEATURED
    let cellIdentifier = "CouponFeaturedCollectionViewCell"
    let cellIdentifier2 = "CouponPercentOffCollectionViewCell"
    let cellIdentifier3 = "CouponLatestCollectionViewCell"
    
    var couponsArray: [Coupon]? = [Coupon]()
    
    var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.resetNavigationBar()
        self.setTitleLogo()
        categoriesButton()
        settingsButton()
        
        self.txtSearch.AddImage(direction: .Left, imageName: "%Off_search", Frame: CGRect(x: 0, y: 0, width: 20, height: 20), backgroundColor: UIColor.clear)
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        let nib2 = UINib(nibName: cellIdentifier2, bundle: nil)
        let nib3 = UINib(nibName: cellIdentifier3, bundle: nil)
        
        self.collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        self.collectionView.register(nib2, forCellWithReuseIdentifier: cellIdentifier2)
        self.collectionView.register(nib3, forCellWithReuseIdentifier: cellIdentifier3)
        
        
        self.collectionView.alwaysBounceVertical = true
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(self.refreshStream), for: .valueChanged)
        
        collectionView!.addSubview(refresher)
        
        self.getCoupons(type: "featured")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:-  Navigation Bar Button Code
    
    func categoriesButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setTitle("Categories", for: .normal)
        leftBtn.addTarget(self, action: #selector(self.categoriesBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func categoriesBtnPressed() {
        openLeft()
    }
    
    func settingsButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        rightBtn.setTitle("Settings", for: .normal)
        rightBtn.addTarget(self, action: #selector(self.settingsBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func settingsBtnPressed() {
        openRight()
    }
    
    @objc func refreshStream() {
        
        print("refresh")
        
        self.couponsArray?.removeAll()
        self.collectionView?.reloadData()
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.getCoupons(type: "featured")
            break
        case 1:
            self.getCoupons(type: "dollaroff")
            break
        case 2:
            self.getCoupons(type: "percentoff")
            break
        case 3:
            self.getCoupons(type: "latest")
            break
        default: break
        }
        
    }
    
    // MARK: - IBAction
    
    @IBAction func segmentedControlAction(_ sender: UISegmentedControl) {
        
        self.couponsArray?.removeAll()
        self.collectionView.reloadData()
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.selectedType = .FEATURED
            self.getCoupons(type: "featured")
            break
        case 1:
            self.selectedType = .DOLLAR_OFF
            self.getCoupons(type: "dollaroff")
            break
        case 2:
            self.selectedType = .PERCENT_OFF
            self.getCoupons(type: "percentoff")
            break
        case 3:
            self.selectedType = .LATEST
            self.getCoupons(type: "latest")
            break
        default: break
        }
    }
}

//MARK:-  UICollectionViewDelegate, UICollectionViewDataSource

extension CouponViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.couponsArray?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.selectedType == .FEATURED {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CouponFeaturedCollectionViewCell
            
            let obj = self.couponsArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.imglink!)
            cell.imgIcon.setImageFromUrl(urlStr: obj.businessLogo!)
            cell.lblPrice.text = "AED \(obj.currentAmount!)"
            cell.lblActualPrice.text = "AED \(obj.currentAmount!)"
            cell.lblDiscountPrice.text = "AED \(obj.totalAmount!)"
            cell.lblPercentOff.text = "AED \(obj.saveAmount!)"
            cell.lblDescription.text = obj.descriptionValue!
            cell.lblTitle.text = obj.coupan!
            cell.lblPercent.text = obj.save!
            
            return cell
        } else if self.selectedType == .DOLLAR_OFF {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier2, for: indexPath) as! CouponPercentOffCollectionViewCell
            
            let obj = self.couponsArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.imglink!)
            cell.imgIcon.setImageFromUrl(urlStr: obj.businessLogo!)
            cell.lblPrice.text = "AED \(obj.totalAmount!)"
            cell.lblActualPrice.text = "AED \(obj.currentAmount!)"
            cell.lblDescription.text = obj.descriptionValue!
            
            return cell
        } else if self.selectedType == .PERCENT_OFF {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier2, for: indexPath) as! CouponPercentOffCollectionViewCell
            
            let obj = self.couponsArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.imglink!)
            cell.imgIcon.setImageFromUrl(urlStr: obj.businessLogo!)
            cell.lblPrice.text = "AED \(obj.totalAmount!)"
            cell.lblActualPrice.text = "AED \(obj.currentAmount!)"
            cell.lblDescription.text = obj.descriptionValue!
            
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier3, for: indexPath) as! CouponLatestCollectionViewCell
            
            let obj = self.couponsArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.imglink!)
            cell.imgIcon.setImageFromUrl(urlStr: obj.businessLogo!)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.selectedType == .FEATURED {
            let numberOfItems: CGFloat = 1.0
            let padding: CGFloat = 0.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = (collectionViewSize.width / numberOfItems - padding)
            collectionViewSize.height = 198//collectionViewSize.height / 2.5
            return collectionViewSize
        } else if self.selectedType == .DOLLAR_OFF {
            let numberOfItems: CGFloat = 3.0
            let padding: CGFloat = 5.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
            collectionViewSize.height = 280//collectionViewSize.height / 2.5
            return collectionViewSize
        } else if self.selectedType == .PERCENT_OFF {
            let numberOfItems: CGFloat = 3.0
            let padding: CGFloat = 5.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
            collectionViewSize.height = 280//collectionViewSize.height / 2.5
            return collectionViewSize
        } else {
            let numberOfItems: CGFloat = 3.0
            let padding: CGFloat = 5.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
            collectionViewSize.height = 244//collectionViewSize.height / 2.5
            return collectionViewSize
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        let obj = self.couponsArray![indexPath.row]
        let vc = CouponDetailViewController.instantiateFromStoryboard()
        vc.coupon = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:-  UITextFieldDelegate

extension CouponViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

//MARK:-  Service Methods

extension CouponViewController {
    
    func getCoupons(type: String) {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        CouponServices.GetCoupons(param: ["coupanType": type], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            self.refresher?.endRefreshing()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let couponResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in couponResponseArray! {
                let obj = Coupon(json: resultObj)
                self.couponsArray?.append(obj)
            }
            
            self.collectionView.reloadData()
            
        })
    }
}
