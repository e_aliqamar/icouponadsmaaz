//
//  CouponDetailViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 13/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class CouponDetailViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> CouponDetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CouponDetailViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    
    let cellIdentifier = "CouponDetailTableViewCell"
    
    var couponDetailArray: [CouponDetail]? = [CouponDetail]()
    var coupon: Coupon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Coupons"
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        self.navigationController?.resetNavigationBar()
        
        backButton()
        shareButton()
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
        
        self.getCoupon()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func shareButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        rightBtn.setImage(UIImage(named: "share_white"), for: .normal)
        rightBtn.addTarget(self, action: #selector(self.shareBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func shareBtnPressed() {
        self.share(text: self.coupon.descriptionValue!)
    }
    
    func share(text: String) {
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop,UIActivityType.postToWeibo,UIActivityType.message,UIActivityType.mail,UIActivityType.print,UIActivityType.copyToPasteboard,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList, UIActivityType.postToFlickr, UIActivityType.openInIBooks,UIActivityType.postToVimeo,UIActivityType.postToTencentWeibo,UIActivityType.markupAsPDF]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension CouponDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 460
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.couponDetailArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CouponDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CouponDetailTableViewCell
        
        let obj = self.couponDetailArray![indexPath.row]
        
        cell.imgBanner.setImageFromUrl(urlStr: obj.image!)
        cell.imgLogo.setImageFromUrl(urlStr: obj.businessLogo!)
        
        cell.lblDate.text = obj.validDate!
        cell.lblAmount.text = "AED \(obj.totalAmount!)"
        cell.txtShortDescription.text = obj.bDesc!
        cell.txtLongDescription.text = obj.descriptionValue!
        
        cell.indexPath = indexPath
        cell.delegate = self
        
        if obj.isInCart == "1" {
            cell.btnAddToCart.setTitle("Allready in Cart", for: .normal)
        } else {
            cell.btnAddToCart.setTitle("Add to Cart", for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
    }
}


//MARK:-  UIButtonCellDelegate

extension CouponDetailViewController: ButtonClickDelegate {
    func click(indexPath: IndexPath) {
        let obj = self.couponDetailArray![indexPath.row]
        if obj.isInCart == "1" {
            Alert.showAlert(title: "", message: "This coupon is allready in cart.")
        } else {
            self.addToCart(indexPath: indexPath)
        }
    }
}

//MARK:-  Service Methods

extension CouponDetailViewController {
    
    func getCoupon() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        let parameters = ["t_coupan_id": self.coupon.tCoupanId!,
                          "t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!]
        CouponServices.GetSingleCoupon(param: parameters, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let couponsResponse = CouponDetail(object: (response?["data"])!)
            self.couponDetailArray?.append(couponsResponse)
            
//            let couponsResponseArray = response?["data"].arrayValue
//            //            print(couponsResponseArray!)
//            for resultObj in couponsResponseArray! {
//                let obj = CouponDetail(json: resultObj)
//                self.couponDetailArray?.append(obj)
//            }
//
            self.tableView.reloadData()
            
        })
    }
    
    func addToCart(indexPath: IndexPath) {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        
        let parameter = ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!,
                         "t_item_id": self.coupon.tCoupanId!,
                         "t_item_type": "coupan"]
        
        CouponServices.AddToCart(param: parameter, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            self.couponDetailArray?[indexPath.row].isInCart = "1"
            self.tableView.reloadData()
            
            let msg = response?["message"].stringValue
            Alert.showAlert(title: "Success", message: msg!)
        })
    }
    
}
