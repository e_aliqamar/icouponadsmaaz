//
//  HomeViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import AVKit
import FSPagerView
import SKActivityIndicatorView

class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pagerView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    // MARK: - Properties
    
    var selectedType: HOMETYPE = .HOTDEALS
    let cellIdentifier = "SliderFSPagerViewCell"
    let cellIdentifier2 = "HotDealsTableViewCell"
    let cellIdentifier3 = "InStoreTableViewCell"
    
    var videosArray: [Video]? = [Video]()
    var hotDealsArray: [HotDeals]? = [HotDeals]()
    var inStoreArray: [InStore]? = [InStore]()
    var savedealsArray: [SaveDeals]? = [SaveDeals]()
    var categoriesArray: [Category]? = [Category]()
    
    var refresher: UIRefreshControl!
    var fsPpagerView: FSPagerView!
    
    var visual: UIVisualEffectView!
    var nibView: FindMe!
    var nibViewCategory: Categories!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationController?.resetNavigationBar()
        self.setTitleLogo()
        categoriesButton()
        settingsButton()
        
        let frame1 = CGRect(x: 0, y: 0, width: self.pagerView.frame.width, height: self.pagerView.frame.height)
        let frame2 = CGRect(x: self.pagerView.center.x, y: self.pagerView.frame.height - 44, width: self.pagerView.frame.width, height: 44)
        
        // Create a pager view
        fsPpagerView = FSPagerView(frame: frame1)
        fsPpagerView.dataSource = self
        fsPpagerView.delegate = self
        let transform = CGAffineTransform(scaleX: 0.75, y: 0.9)
        fsPpagerView.itemSize = self.pagerView.frame.size.applying(transform)
        fsPpagerView.transformer = FSPagerViewTransformer.init(type: .linear)
        fsPpagerView.isInfinite = true
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        fsPpagerView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        
        self.pagerView.addSubview(fsPpagerView)
        // Create a page control
        let pageControl = FSPageControl(frame: frame2)
        self.pagerView.addSubview(pageControl)
        
        self.txtSearch.AddImage(direction: .Left, imageName: "%Off_search", Frame: CGRect(x: 0, y: 0, width: 20, height: 20), backgroundColor: UIColor.clear)
        
        
        let nib2 = UINib(nibName: cellIdentifier2, bundle: nil)
        let nib3 = UINib(nibName: cellIdentifier3, bundle: nil)
        
        self.tableView.register(nib2, forCellReuseIdentifier: cellIdentifier2)
        self.tableView.register(nib3, forCellReuseIdentifier: cellIdentifier3)
        self.tableView.tableFooterView = UIView()
        
        self.tableView.alwaysBounceVertical = true
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(self.refreshStream), for: .valueChanged)
        
        tableView!.addSubview(refresher)
        
        self.segmentedControl.isHidden = true
        
        self.getCategories()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:-  Navigation Bar Button Code
    
    func categoriesButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setTitle("Categories", for: .normal)
        leftBtn.addTarget(self, action: #selector(self.categoriesBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func categoriesBtnPressed() {
        openLeft()
    }
    
    func settingsButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        rightBtn.setTitle("Settings", for: .normal)
        rightBtn.addTarget(self, action: #selector(self.settingsBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func settingsBtnPressed() {
        openRight()
    }
    
    @objc func refreshStream() {
        
        print("refresh")
        self.hotDealsArray?.removeAll()
        self.inStoreArray?.removeAll()
        self.savedealsArray?.removeAll()
        
        self.tableView?.reloadData()
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            SKActivityIndicator.show("", userInteractionStatus: false)
            self.getHotDeals {
                SKActivityIndicator.dismiss()
            }
            break
        case 1:
            self.getInStoreDeals()
            break
        case 2:
            self.getSaveDeals()
            break
        default: break
        }
        
    }
    
    func servicesGroup() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        let group = DispatchGroup()
        group.enter()
        self.getVideos(completionHandler: {() in
            group.leave()
        })
        
        group.enter()
        self.getHotDeals(completionHandler: {() in
            group.leave()
        })
        
        // does not wait. But the code in notify() gets run
        // after enter() and leave() calls are balanced
        
        group.notify(queue: .main) {
            print("fetch both")
            SKActivityIndicator.dismiss()
            self.segmentedControl.isHidden = false
        }
    }
    
    func share(text: String) {
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop,UIActivityType.postToWeibo,UIActivityType.message,UIActivityType.mail,UIActivityType.print,UIActivityType.copyToPasteboard,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList, UIActivityType.postToFlickr, UIActivityType.openInIBooks,UIActivityType.postToVimeo,UIActivityType.postToTencentWeibo,UIActivityType.markupAsPDF]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func dismissVisualEffect() {
        if self.visual != nil {
            self.visual.removeFromSuperview()
            self.visual = nil
        }
        if self.nibView != nil {
            self.nibView.removeFromSuperview()
            self.nibView = nil
        }
        if self.nibViewCategory != nil {
            self.nibViewCategory.removeFromSuperview()
            self.nibViewCategory = nil
        }
        
        
        self.servicesGroup()
    }
    
    // MARK: - IBAction
    
    @IBAction func segmentedControlAction(_ sender: UISegmentedControl) {
        
        self.hotDealsArray?.removeAll()
        self.inStoreArray?.removeAll()
        self.savedealsArray?.removeAll()
        
        self.tableView?.reloadData()
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.selectedType = .HOTDEALS
            SKActivityIndicator.show("", userInteractionStatus: false)
            self.getHotDeals {
                SKActivityIndicator.dismiss()
            }
            break
        case 1:
            self.selectedType = .INSTORE
            self.getInStoreDeals()
            break
        case 2:
            self.selectedType = .SAVEDEALS
            self.getSaveDeals()
            break
        default: break
        }
    }
}

//MARK:-  FSPagerViewDataSource and FSPagerViewDelegate

extension HomeViewController: FSPagerViewDataSource, FSPagerViewDelegate {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return (self.videosArray?.count)!
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, at: index) as! SliderFSPagerViewCell
        
        let obj = self.videosArray![index]
        
        cell.imageBanner.setImageFromUrl(urlStr: obj.logoImage!)
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        print("Selected row: \(index)")
        
        let obj = self.videosArray![index]
        
        let videoURL = URL(string: obj.businessVideo!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.selectedType == .HOTDEALS {
            return (self.hotDealsArray?.count)!
        } else if self.selectedType == .INSTORE {
            return (self.inStoreArray?.count)!
        } else {
            return (self.savedealsArray?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.selectedType == .HOTDEALS {
            
            let cell: HotDealsTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier2, for: indexPath) as! HotDealsTableViewCell
            
            let obj = self.hotDealsArray![indexPath.row]
            
            cell.imgDisplay.setImageFromUrl(urlStr: obj.displayImage!)
            cell.imgLogo.setImageFromUrl(urlStr: obj.logoImage!)
            cell.lblUrl.text = obj.dealWebsite!
            cell.txtDescription.text = obj.dealDescription!
            
            if obj.isSave == 1 {
                cell.btnSave.setImage(UIImage(named: "deal_saved_icon"), for: .normal)
            } else {
                cell.btnSave.setImage(UIImage(named: "save_icon"), for: .normal)
            }
            
            cell.delegate = self
            cell.indexPath = indexPath
            
            return cell
        } else if self.selectedType == .INSTORE {
            
            let cell: InStoreTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier3, for: indexPath) as! InStoreTableViewCell
            
            let obj = self.inStoreArray![indexPath.row]
            
            cell.imgLogo.setImageFromUrl(urlStr: obj.logoImage!)
            cell.lblUrl.text = obj.businessWebsite!
            cell.txtDescription.text = obj.businessDescription!
            if obj.noOfDeals! > 1 {
                cell.lblDealsCount.text = "\(obj.noOfDeals!) Active Deals"
            } else {
                cell.lblDealsCount.text = "\(obj.noOfDeals!) Active Deal"
            }
            
            cell.delegate = self
            cell.indexPath = indexPath
            
            return cell
        } else {
            
            let cell: HotDealsTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier2, for: indexPath) as! HotDealsTableViewCell
            
            let obj = self.savedealsArray![indexPath.row]
            
            cell.imgDisplay.setImageFromUrl(urlStr: obj.displayImage!)
            cell.imgLogo.setImageFromUrl(urlStr: obj.displayImage!)
            cell.lblUrl.text = obj.dealWebsite!
            cell.txtDescription.text = obj.dealDescription!
            
            cell.btnSave.setImage(UIImage(named: "delete_icon"), for: .normal)
            
            cell.delegate = self
            cell.indexPath = indexPath
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
    }
}

//MARK:-  UITextFieldDelegate

extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

//MARK:-  UIButtonCellDelegate

extension HomeViewController: ButtonClickDelegate {
    func shareClick(indexPath: IndexPath) {
        if self.selectedType == .HOTDEALS {
            
            let obj = self.hotDealsArray![indexPath.row]
            self.share(text: obj.businessWebsite!)
            
        } else if self.selectedType == .INSTORE {
            
            let obj = self.inStoreArray![indexPath.row]
            self.share(text: obj.businessWebsite!)
            
        } else {
            
            let obj = self.savedealsArray![indexPath.row]
            self.share(text: obj.dealWebsite!)
            
        }
    }
    
    func locationClick(indexPath: IndexPath) {
        if self.selectedType == .HOTDEALS {
            
            let obj = self.hotDealsArray![indexPath.row]
            let vc = LocationViewController.instantiateFromStoryboard()
            vc.detailHotDeals = obj
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if self.selectedType == .INSTORE {
            
            let obj = self.inStoreArray![indexPath.row]
            let vc = LocationViewController.instantiateFromStoryboard()
            vc.detailInStoreDeals = obj
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            
            let obj = self.savedealsArray![indexPath.row]
            let vc = LocationViewController.instantiateFromStoryboard()
            vc.detailSaveDeals = obj
            self.navigationController?.pushViewController(vc, animated: true)   
        }
        
    }
    
    func saveClick(indexPath: IndexPath) {
        if self.selectedType == .HOTDEALS {
            
            let obj = self.hotDealsArray![indexPath.row]
            if obj.isSave == 0 {
                self.saveDeal(id: obj.tDealId!) { (status) in
                    if status {
                        self.hotDealsArray![indexPath.row].isSave = 1
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                    } else {
                        
                    }
                }
            } else {
                self.unSaveDeal(id: obj.tDealId!) { (status) in
                    if status {
                        self.hotDealsArray![indexPath.row].isSave = 0
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                    } else {
                        
                    }
                }
            }
            
        } else {
            
            let obj = self.savedealsArray![indexPath.row]
            self.unSaveDeal(id: obj.tDealId!) { (status) in
                if status {
                    self.savedealsArray!.remove(at: indexPath.row)
                    self.tableView.reloadData()
                } else {
                    
                }
            }
        }
    }
    
    func playClick(indexPath: IndexPath) {
        let obj = self.inStoreArray![indexPath.row]
        
        let videoURL = URL(string: obj.businessVideo!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func detailClick(indexPath: IndexPath) {
        if self.selectedType == .HOTDEALS {
            
            let obj = self.hotDealsArray![indexPath.row]
            
            let vc = DetailViewController.instantiateFromStoryboard()
            vc.detailHotDeals = obj
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if self.selectedType == .INSTORE {
            
            let obj = self.inStoreArray![indexPath.row]
            
            let vc = DetailInstoreViewController.instantiateFromStoryboard()
            vc.detailInStoreDeals = obj
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            
            let obj = self.savedealsArray![indexPath.row]
            
            let vc = DetailViewController.instantiateFromStoryboard()
            vc.detailSaveDeals = obj
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
}

//MARK:-  FindMeDelegate

extension HomeViewController: FindMeDelegate {
    func findMe(zipCode: String) {
        if let nibView = Categories.instantiateFromNib() {
            self.nibViewCategory = nibView
            self.nibViewCategory.delegate = self
            self.nibViewCategory.categories = self.categoriesArray!
            
            var height = CGFloat(44 * (self.categoriesArray?.count)!) + 136
            let width = CGFloat(self.containerView.frame.size.width - 40)
            
            if height > self.containerView.frame.size.height {
                height = self.containerView.frame.size.height - 40
            }
            
            let frame = CGRect(x: 0, y: 0, width: width, height: height)
            
            self.nibViewCategory.registerNib()
            self.presentNib(nib: self.nibViewCategory, with: frame, in: self.containerView)
            
        }
    }
}

//MARK:-  CategoriesDelegate

extension HomeViewController: CategoriesDelegate {
    func skip() {
        self.visual.removeFromSuperview()
        self.visual = nil
        
        self.servicesGroup()
    }
    
    func next(indexes: [IndexPath]) {
        self.visual.removeFromSuperview()
        self.visual = nil
        
        self.servicesGroup()
    }
    
}

//MARK:-  Service Methods

extension HomeViewController {
    
    func getVideos(completionHandler: @escaping () -> Void) {
        
        HomeServices.GetVideos(param: [:], completionHandler: {(status, response, error) in
            
            self.refresher?.endRefreshing()
            completionHandler()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let videoResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in videoResponseArray! {
                let obj = Video(json: resultObj)
                self.videosArray?.append(obj)
            }
            
            self.fsPpagerView.reloadData()
            
        })
    }
    
    func getHotDeals(completionHandler: @escaping () -> Void) {
        
        HomeServices.GetHotDeals(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!], completionHandler: {(status, response, error) in
            
            self.refresher?.endRefreshing()
            completionHandler()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                Alert.showAlert(title: "Error", message: msg!)
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let hotDealsResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in hotDealsResponseArray! {
                let obj = HotDeals(json: resultObj)
                self.hotDealsArray?.append(obj)
            }
            
            self.tableView.reloadData()
            
        })
    }
    
    func getInStoreDeals() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        HomeServices.GetInStoreDeals(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            self.refresher?.endRefreshing()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let inStoreResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in inStoreResponseArray! {
                let obj = InStore(json: resultObj)
                self.inStoreArray?.append(obj)
            }
            
            self.tableView.reloadData()
            
        })
    }
    
    func getSaveDeals() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        HomeServices.GetSaveDeals(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            self.refresher?.endRefreshing()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let saveDealsResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in saveDealsResponseArray! {
                let obj = SaveDeals(json: resultObj)
                self.savedealsArray?.append(obj)
            }
            
            self.tableView.reloadData()
            
        })
    }
    
    func saveDeal(id: String, completionHandler: @escaping (_ status: Bool) -> Void) {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        HomeServices.SaveDeal(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!, "t_deal_id": id], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    completionHandler(false)
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                completionHandler(false)
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            completionHandler(true)
            
        })
    }
    
    func unSaveDeal(id: String, completionHandler: @escaping (_ status: Bool) -> Void) {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        HomeServices.UnSaveDeal(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!, "t_deal_id": id], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    completionHandler(false)
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                completionHandler(false)
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            completionHandler(true)
            
        })
    }
    
    func getCategories() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        HomeServices.GetCategories(param: [:], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let categoriesResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in categoriesResponseArray! {
                let obj = Category(json: resultObj)
                self.categoriesArray?.append(obj)
            }
            
            if self.visual != nil {
                return
            }
            
            if let nibView = FindMe.instantiateFromNib() {
                self.nibView = nibView
                self.nibView.delegate = self
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissVisualEffect))
                
                self.visual = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
                //        let blur = UIBlurEffect(style: .dark)
                //        let vibrancy = UIVibrancyEffect(blurEffect: blur)//UIVibrancyEffect(forBlurEffect: blur)
                
                //        visualEffectView.effect = vibrancy
                
                
                self.visual = self.presentNib(nib: self.nibView, with: self.visual, in: self.containerView)
                self.visual.addGestureRecognizer(tapGesture)
            }
            
        })
    }
}
