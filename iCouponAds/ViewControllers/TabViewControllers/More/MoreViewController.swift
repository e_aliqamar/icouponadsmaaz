//
//  MoreViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 12/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController {

    class func instantiateFromStoryboard() -> MoreViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MoreViewController
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    
    let cellIdentifier = "MyCardsTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.resetNavigationBar()
        self.setTitleLogo()
        categoriesButton()
        settingsButton()
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:-  Navigation Bar Button Code
    
    func categoriesButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setTitle("Categories", for: .normal)
        leftBtn.addTarget(self, action: #selector(self.categoriesBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func categoriesBtnPressed() {
        openLeft()
    }
    
    func settingsButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        rightBtn.setTitle("Settings", for: .normal)
        rightBtn.addTarget(self, action: #selector(self.settingsBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func settingsBtnPressed() {
        openRight()
    }
}

extension MoreViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: MyCardsTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyCardsTableViewCell
        
        switch indexPath.row {
        case 0:
            cell.lblTitle.text = "Clippings"
            cell.lblPrice.isHidden = true
        case 1:
            cell.lblTitle.text = "My Cards"
            cell.lblPrice.isHidden = true
        case 2:
            cell.lblTitle.text = "My Cart"
            cell.lblPrice.isHidden = true
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        
        switch indexPath.row {
        case 0:
            let vc = ClippingViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = MyCardsViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = GetCartViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
            break
        default:
            break
        }
    }
}
