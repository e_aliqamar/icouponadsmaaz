//
//  ShoppingListViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class ShoppingListViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ShoppingListViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ShoppingListViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewClippings: UITableView!
    @IBOutlet weak var addItemView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    // MARK: - Properties
    
    let cellIdentifier = "ShoppingListTableViewCell"
    let cellIdentifier2 = "ClippingTableViewCell"
    var selectedType: SHOPPINGLISTTYPE = .SHOPPINGLIST
    var checked: [Bool] = [Bool]()
    
    var clippingsArray: [Clipping]? = [Clipping]()
    var refresher: UIRefreshControl!
    
    let data = [["Bread", "Butter", "Rusk"], ["Adidas", "Nike", "Puma"]]
    let headerTitles = ["Bakery", "Sports"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationController?.resetNavigationBar()
        self.setTitleLogo()
        editButton()
        
        self.addItemView.layer.addBorder(edge: .top, color: UIColor.lightGray, thickness: 0.5)
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
        
        let nib2 = UINib(nibName: cellIdentifier2, bundle: nil)
        
        self.tableViewClippings.register(nib2, forCellReuseIdentifier: cellIdentifier2)
        self.tableViewClippings.tableFooterView = UIView()
        
        self.tableViewClippings.alwaysBounceVertical = true
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(self.refreshStream), for: .valueChanged)
        
        tableViewClippings!.addSubview(refresher)
        tableViewClippings.isHidden = true
        
        resetChecks()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        self.segmentedControl.setTitle("Shopping List", forSegmentAt: 0)
//        self.segmentedControl.setTitle("Clippings", forSegmentAt: 1)
//    }
    
    //MARK:-  Navigation Bar Button Code
    
    func editButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        rightBtn.setTitle("Edit", for: .normal)
        rightBtn.addTarget(self, action: #selector(self.editBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func editBtnPressed() {
        
    }
    
    func resetChecks() {
        for _ in 0..<3 {
            self.checked.append(false)
        }
    }
    
    @objc func refreshStream() {
        
        print("refresh")
        
        self.clippingsArray?.removeAll()
        self.tableViewClippings?.reloadData()
        
        self.getClippings()
    }
    
    // MARK: - IBAction
    
    @IBAction func segmentedControlAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.selectedType = .SHOPPINGLIST
            self.tableViewClippings.isHidden = true
            self.containerView.isHidden = false
            self.clippingsArray?.removeAll()
            self.tableViewClippings.reloadData()
            break
        case 1:
            self.selectedType = .CLIPPINGLIST
            self.tableViewClippings.isHidden = false
            self.containerView.isHidden = true
            self.getClippings()
            break
        default: break
        }
    }
    
    @IBAction func addItemAction(_ sender: UIButton) {
        let vc = AddCategoryViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension ShoppingListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (tableView == self.tableView) ? self.data.count : 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.tableView {
            return self.headerTitles[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tableView == self.tableView) ? self.data[section].count : (self.clippingsArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView {
            
            let cell: ShoppingListTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ShoppingListTableViewCell
            
            if !checked[indexPath.row] {
                cell.btnCheckmark.isSelected = false
            } else if checked[indexPath.row] {
                cell.btnCheckmark.isSelected = true
            }
            
            cell.lblTitle.text = self.data[indexPath.section][indexPath.row]
            
            cell.indexPath = indexPath
            cell.delegate = self
            
            return cell
        } else {
            let cell: ClippingTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier2, for: indexPath) as! ClippingTableViewCell
            
            let obj = self.clippingsArray![indexPath.row]

            cell.clipping = obj
            cell.collectionView.reloadData()
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        
        if tableView == self.tableView {
            if let cell = tableView.cellForRow(at: indexPath) as? ShoppingListTableViewCell {
                
                if cell.btnCheckmark.isSelected {
                    cell.btnCheckmark.isSelected = false
                    checked[indexPath.row] = false
                } else {
                    cell.btnCheckmark.isSelected = true
                    checked[indexPath.row] = true
                }
            }
        } else {
            
        }
 
    }
}

//MARK:-  UIButtonCellDelegate

extension ShoppingListViewController: ButtonClickDelegate {
    func click(indexPath: IndexPath) {
        let vc = SearchViewController.instantiateFromStoryboard()
        vc.title = self.data[indexPath.section][indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)   
    }
}

//MARK:-  Service Methods

extension ShoppingListViewController {
    
    func getClippings() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        ClippingServices.GetClippings(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            self.refresher?.endRefreshing()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let clippingResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in clippingResponseArray! {
                let obj = Clipping(json: resultObj)
                self.clippingsArray?.append(obj)
            }
            
            self.tableViewClippings.reloadData()
            
        })
    }
}
