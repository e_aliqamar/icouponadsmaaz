//
//  FlyerViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class FlyerViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> FlyerViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FlyerViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var txtSearch: UITextField!
    
    // MARK: - Properties

    var selectedType: FLYERTYPE = .FEATURED
    let cellIdentifier = "FlyersFeaturedCollectionViewCell"
    let cellIdentifier2 = "LatestCollectionViewCell"
    
    var flyersArray: [Flyer]? = [Flyer]()
    var flyersFeaturedArray: [Flyer]? = [Flyer]()
    var flyersLetestArray: [Flyer]? = [Flyer]()
    var flyersFavoriteArray: [Flyer]? = [Flyer]()
    
    var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationController?.resetNavigationBar()
        self.setTitleLogo()
        categoriesButton()
        settingsButton()
        
        self.txtSearch.AddImage(direction: .Left, imageName: "%Off_search", Frame: CGRect(x: 0, y: 0, width: 20, height: 20), backgroundColor: UIColor.clear)
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        let nib2 = UINib(nibName: cellIdentifier2, bundle: nil)
        
        self.collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        self.collectionView.register(nib2, forCellWithReuseIdentifier: cellIdentifier2)
        
        self.collectionView.alwaysBounceVertical = true
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(self.refreshStream), for: .valueChanged)
        
        collectionView!.addSubview(refresher)
        
        self.getFlyers()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:-  Navigation Bar Button Code
    
    func categoriesButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setTitle("Categories", for: .normal)
        leftBtn.addTarget(self, action: #selector(self.categoriesBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func categoriesBtnPressed() {
        openLeft()
    }
    
    func settingsButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        rightBtn.setTitle("Settings", for: .normal)
        rightBtn.addTarget(self, action: #selector(self.settingsBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func settingsBtnPressed() {
        openRight()
    }
    
    @objc func refreshStream() {
        
        print("refresh")
        self.flyersArray?.removeAll()
        self.flyersLetestArray?.removeAll()
        self.flyersFavoriteArray?.removeAll()
        self.flyersFeaturedArray?.removeAll()
        
        self.collectionView?.reloadData()
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.getFlyers()
            break
        case 1:
            self.getFlyers()
            break
        case 2:
            self.getFlyers()
            break
        case 3:
            self.getFavoriteFlyers(id: (Singleton.sharedInstance.CurrentUser?.userId!)!)
            break
        default: break
        }
        
    }
    
    // MARK: - IBAction
    
    @IBAction func segmentedControlAction(_ sender: UISegmentedControl) {
        self.flyersArray?.removeAll()
        self.flyersFeaturedArray?.removeAll()
        self.flyersLetestArray?.removeAll()
        self.flyersFavoriteArray?.removeAll()
        self.collectionView.reloadData()
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.selectedType = .FEATURED
            self.getFlyers()
            break
        case 1:
            self.selectedType = .A_Z
            self.getFlyers()
            break
        case 2:
            self.selectedType = .LATEST
            self.getFlyers()
            break
        case 3:
            self.selectedType = .FAVORITE
            self.getFavoriteFlyers(id: (Singleton.sharedInstance.CurrentUser?.userId!)!)
            break
        default: break
        }
    }
}

//MARK:-  UICollectionViewDelegate, UICollectionViewDataSource

extension FlyerViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.selectedType == .FEATURED {
            return (self.flyersFeaturedArray?.count)!
        } else if self.selectedType == .A_Z {
            return (self.flyersArray?.count)!
        } else if self.selectedType == .LATEST {
            return (self.flyersLetestArray?.count)!
        } else {
            return (self.flyersFavoriteArray?.count)!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.selectedType == .FEATURED {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! FlyersFeaturedCollectionViewCell
            
            let obj = self.flyersFeaturedArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.image!)
            cell.imgLogo.setImageFromUrl(urlStr: obj.businessLogo!)
            
            if obj.isNew == "1" {
                cell.viewIsNew.isHidden  = false
            } else {
                cell.viewIsNew.isHidden  = true
            }
            
            cell.lblTitle.text = obj.descriptionValue!
            cell.lblDaysLeft.text = "\((obj.expiryDate!.toDate().daysLeft())) left"
            
            return cell
        } else if self.selectedType == .A_Z {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier2, for: indexPath) as! LatestCollectionViewCell
            
            let obj = self.flyersArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.image!)
            
            if obj.isNew == "1" {
                cell.viewIsNew.isHidden  = false
            } else {
                cell.viewIsNew.isHidden  = true
            }
            
            cell.lblTitle.text = obj.descriptionValue!
            cell.lblFlyer.text = obj.flyer!
            
            return cell
        } else if self.selectedType == .LATEST {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier2, for: indexPath) as! LatestCollectionViewCell
            
            let obj = self.flyersLetestArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.image!)
            
            if obj.isNew == "1" {
                cell.viewIsNew.isHidden  = false
            } else {
                cell.viewIsNew.isHidden  = true
            }
            
            cell.lblTitle.text = obj.descriptionValue!
            cell.lblFlyer.text = obj.flyer!
            
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier2, for: indexPath) as! LatestCollectionViewCell
            
            let obj = self.flyersFavoriteArray![indexPath.row]
            
            cell.imgBanner.setImageFromUrl(urlStr: obj.businessLogo!)
           
            cell.viewIsNew.isHidden  = true
            
            cell.lblTitle.text = obj.descriptionValue!
            cell.lblFlyer.text = obj.flyer!
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.selectedType == .FEATURED {
            let numberOfItems: CGFloat = 1.0
            let padding: CGFloat = 0.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
            collectionViewSize.height = collectionViewSize.height / 2.5
            return collectionViewSize
        } else if self.selectedType == .A_Z {
            let numberOfItems: CGFloat = 3.0
            let padding: CGFloat = 5.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
            collectionViewSize.height = collectionViewSize.height / 2.5
            return collectionViewSize
        } else if self.selectedType == .A_Z {
            let numberOfItems: CGFloat = 3.0
            let padding: CGFloat = 5.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
            collectionViewSize.height = collectionViewSize.height / 2.5
            return collectionViewSize
        } else {
            let numberOfItems: CGFloat = 3.0
            let padding: CGFloat = 5.0
            
            var collectionViewSize = collectionView.frame.size
            collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
            collectionViewSize.height = collectionViewSize.height / 2.5
            return collectionViewSize
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        
        if self.selectedType == .FEATURED {
            let obj = self.flyersFeaturedArray![indexPath.row]
            let vc = FlyerDetailViewController.instantiateFromStoryboard()
            vc.flyer = obj
            self.navigationController?.pushViewController(vc, animated: true)
        } else if self.selectedType == .A_Z {
            let obj = self.flyersArray![indexPath.row]
            let vc = FlyerDetailViewController.instantiateFromStoryboard()
            vc.flyer = obj
            self.navigationController?.pushViewController(vc, animated: true)
        } else if self.selectedType == .LATEST {
            let obj = self.flyersLetestArray![indexPath.row]
            let vc = FlyerDetailViewController.instantiateFromStoryboard()
            vc.flyer = obj
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let obj = self.flyersFavoriteArray![indexPath.row]
            let vc = FlyerDetailViewController.instantiateFromStoryboard()
            vc.flyer = obj
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

//MARK:-  UITextFieldDelegate

extension FlyerViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

//MARK:-  Service Methods

extension FlyerViewController {
    
    func getFlyers() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        FlyerServices.GetFlyers(param: [:], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            self.refresher?.endRefreshing()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
//                        print(response!)
            
            let flyerResponseArray = response?["data"].arrayValue
//            print(flyerResponseArray!)
            for resultObj in flyerResponseArray! {
                let obj = Flyer(json: resultObj)
                self.flyersArray?.append(obj)
            }
            
            self.flyersFeaturedArray = self.flyersArray?.filter { $0.isFeatured == "1" }
            self.flyersLetestArray = self.flyersArray?.filter { $0.isNew == "1" }
            
            self.collectionView.reloadData()
            
        })
    }
    
    func getFavoriteFlyers(id: String) {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        FlyerServices.GetFavoriteFlyers(param: ["t_user_id": id], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            self.refresher?.endRefreshing()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            let flyerResponseArray = response?["data"].arrayValue
            //            print(flyerResponseArray!)
            for resultObj in flyerResponseArray! {
                let obj = Flyer(json: resultObj)
                self.flyersFavoriteArray?.append(obj)
            }
            
            self.collectionView.reloadData()
            
        })
    }
}
