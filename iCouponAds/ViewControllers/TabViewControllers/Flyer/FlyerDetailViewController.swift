//
//  FlyerDetailViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 18/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class FlyerDetailViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> FlyerDetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FlyerDetailViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    
    let cellIdentifier = "CouponDetailTableViewCell"
    
    var flyer: Flyer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.title = self.flyer.descriptionValue!
        self.navigationItem.setTitle(title: self.flyer.descriptionValue!, subtitle:  "\((self.flyer.expiryDate!.toDate().daysLeft())) left")

        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        self.navigationController?.resetNavigationBar()
        
        backButton()
        favoriteButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func favoriteButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let rightBtn = UIButton(type: .custom)
        if self.flyer.isFavourite == 0 {
            rightBtn.setImage(UIImage(named: "Favorite"), for: .normal)
        } else {
            rightBtn.setImage(UIImage(named: "Favorite_fill"), for: .normal)
        }
        rightBtn.addTarget(self, action: #selector(self.favoriteBtnPressed), for: .touchUpInside)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    @objc func favoriteBtnPressed() {
        if self.flyer.isFavourite == 0 {
            self.favoriteFlyer()
        } else {
            self.unfavoriteFlyer()
        }
    }

}

//MARK:-  Service Methods

extension FlyerDetailViewController {
    
    func favoriteFlyer() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        FlyerServices.FavoriteFlyer(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!,
                                            "t_flyer_id": self.flyer.tFlyerId!], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            self.flyer.isFavourite = 1
            self.favoriteButton()
        })
    }
    
    func unfavoriteFlyer() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        FlyerServices.UnfavoriteFlyer(param: ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!,
                                              "t_flyer_id": self.flyer.tFlyerId!], completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            self.flyer.isFavourite = 0
            self.favoriteButton()
        })
    }
}
