//
//  ProfileViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 03/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class ProfileViewController: UIViewController {

    class func instantiateFromStoryboard() -> ProfileViewController {
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProfileViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnEditName: UIButton!
    @IBOutlet weak var btnEditAddress: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Profile"
        backButton()
        
        self.txtName.text = Singleton.sharedInstance.CurrentUser?.name!
        self.txtEmail.text = Singleton.sharedInstance.CurrentUser?.email!
        if Singleton.sharedInstance.CurrentUser?.address != "" {
            self.txtAddress.text = Singleton.sharedInstance.CurrentUser?.address!
        } else {
            self.txtAddress.text = ""
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func updateProfile(parameters: [String: Any]) {
        
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        UserServices.EditProfile(param: parameters, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            UserDefaults.standard.removeObject(forKey: User_data_userDefault)
            UserDefaults.standard.synchronize()
            
            Singleton.sharedInstance.CurrentUser = nil
            
            let userResultObj = UserObject(object: (response?["data"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            let msg = response?["message"].stringValue
            print("Message: \(String(describing: msg!))")
            
            Alert.showAlert(title: "Success", message: msg!)
            
        })
    }
    // MARK: - IBAction
    
    @IBAction func editAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if self.btnEditName.isHidden {
            self.txtName.isEnabled = true
            self.txtAddress.isEnabled = true
            self.btnEditName.isHidden = false
            self.btnEditAddress.isHidden = false
        } else {
            self.txtName.isEnabled = false
            self.txtAddress.isEnabled = false
            self.btnEditName.isHidden = true
            self.btnEditAddress.isHidden = true
        }
    }
    
    @IBAction func editNameAction(_ sender: UIButton) {
        self.txtName.becomeFirstResponder()
    }
    
    @IBAction func editAddressAction(_ sender: UIButton) {
        self.txtAddress.becomeFirstResponder()
    }
    
}

//MARK:-  UITextFieldDelegate

extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField == self.txtName {
            guard let name = txtName.text, AppHelper.isValid(name: name) else {
                print("Please Enter Name")
                
                Alert.showAlert(title: "Error", message: "Name must be 2 to 35 characters long")
                
                return true
            }
            let param = ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!,
                         "user_name": name,
                         "user_email": (Singleton.sharedInstance.CurrentUser?.email!)!,
                         "user_address": (Singleton.sharedInstance.CurrentUser?.address != "") ? (Singleton.sharedInstance.CurrentUser?.address!)! : ""]
            self.updateProfile(parameters: param)
        } else {
            guard let address = txtAddress.text, address != "" else {
                print("Please Enter Address")
                
                Alert.showAlert(title: "Error", message: "Please enter valid address")
                
                return true
            }
            let param = ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!,
                         "user_name": (Singleton.sharedInstance.CurrentUser?.name!)!,
                         "user_email": (Singleton.sharedInstance.CurrentUser?.email!)!,
                         "user_address": address]
            self.updateProfile(parameters: param)
        }
        return true
    }
}
