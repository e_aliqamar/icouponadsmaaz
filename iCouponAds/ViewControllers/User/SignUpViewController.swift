//
//  SignUpViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import RMDateSelectionViewController
import SKActivityIndicatorView

class SignUpViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SignUpViewController {
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignUpViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCPassword: UITextField!
    @IBOutlet weak var btnDatePicker: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//        self.title = "Signup"
        self.backButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - IBAction
    
    @IBAction func signupAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        guard let name = txtName.text, AppHelper.isValid(name: name) else {
            print("Please Enter Name")
            
            Alert.showAlert(title: "Error", message: "Name must be 2 to 35 characters long")
            
            return
        }
        
        guard let emailAddress = txtEmail.text, AppHelper.isValid(email: emailAddress) else {
            print("Enter valid Email address")
            
            Alert.showAlert(title: "Error", message: "Enter valid Email address")
            
            return
        }
        
        guard let password = txtPassword.text, AppHelper.isValid(password: password) else {
            print("Please Enter Password")
            
            Alert.showAlert(title: "Error", message: "Password must be 6 to 16 characters long")
            
            return
        }
        
        guard let cPassword = txtCPassword.text, cPassword != "" else {
            print("Please Enter Password")
            
            Alert.showAlert(title: "Error", message: "Please Enter Password")
            
            return
        }
        
        if password != cPassword {
            print("Password not matched")
            
            Alert.showAlert(title: "Error", message: "Password not matched")
            
            return
        }
        
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        var parameters = ["user_name": name, "user_email": emailAddress, "user_password": password]
        
        if let dob = txtDob.text {
            if dob != "" {
                parameters["user_dob"] = dob
            }
        }
        
        UserServices.Register(param: parameters, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            print(response!)
            
            
            let userResultObj = UserObject(object: (response?["data"])!)
            Singleton.sharedInstance.CurrentUser = userResultObj
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            let vc = UIStoryboard(name: "SideMenu", bundle: Bundle.main).instantiateViewController(withIdentifier: "RootViewController")
            UIWINDOW?.rootViewController = vc
            
        })
    }
    
    @IBAction func datePickerAction(_ sender: UIButton) {
        
        let style = RMActionControllerStyle.white
        
        let selectAction = RMAction<UIDatePicker>(title: "Select", style: RMActionStyle.done) { controller in
            print("Successfully selected date: ", controller.contentView.date);
            self.txtDob.text = AppHelper.formateDate(controller.contentView.date, dateFormat: "dd/MM/YYYY")
        }
        
        let cancelAction = RMAction<UIDatePicker>(title: "Cancel", style: RMActionStyle.cancel) { _ in
            print("Date selection was canceled")
        }
        
        let actionController = RMDateSelectionViewController(style: style, title: "Test", message: "You must be 10 years old or above to use this app. \nPlease choose a date and press 'Select' or 'Cancel'.", select: selectAction, andCancel: cancelAction)!;
        
        actionController.datePicker.datePickerMode = .date;
        actionController.datePicker.minuteInterval = 5;
        actionController.datePicker.date = Calendar.current.date(byAdding: .year, value: -10, to: Date())!
        
        if actionController.responds(to: Selector(("popoverPresentationController:"))) && UIDevice.current.userInterfaceIdiom == .pad {
            
            actionController.modalPresentationStyle = UIModalPresentationStyle.popover
            
            if let popoverPresentationController = actionController.popoverPresentationController {
                popoverPresentationController.sourceView = self.btnDatePicker
                popoverPresentationController.sourceRect = self.btnDatePicker.frame
            }
        }
        
        present(actionController, animated: true, completion: nil)
        
    }
    
}

//MARK:-  UITextFieldDelegate

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtName {
            txtEmail.becomeFirstResponder()
        } else if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        } else if textField == txtPassword {
            txtCPassword.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        return true
    }
}
