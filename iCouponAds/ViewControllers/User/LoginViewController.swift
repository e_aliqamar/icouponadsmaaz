//
//  LoginViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class LoginViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//        self.title = "Login"
        self.navigationController?.transparentNavigationBar()
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - IBAction
    
    @IBAction func loginAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        guard let emailAddress = txtEmail.text, AppHelper.isValid(email: emailAddress) else {
            print("Enter valid Email address")
            
            Alert.showAlert(title: "Error", message: "Enter valid Email address")
            
            return
        }
        
        guard let password = txtPassword.text, AppHelper.isValid(password: password) else {
            print("Please Enter Password")
            
            Alert.showAlert(title: "Error", message: "Password must be 6 to 16 characters long")
            
            return
        }
        
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        let parameters = ["user_email": emailAddress, "user_password": password]
        UserServices.Login(param: parameters, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            print(response!)
            
            let userResultObj = UserObject(object: (response?["data"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            let vc = UIStoryboard(name: "SideMenu", bundle: Bundle.main).instantiateViewController(withIdentifier: "RootViewController")
            UIWINDOW?.rootViewController = vc
        })
    }
    
    @IBAction func signupAction(_ sender: UIButton) {
        let vc = SignUpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        guard let emailAddress = txtEmail.text, AppHelper.isValid(email: emailAddress) else {
            print("Enter valid Email address")
            
            Alert.showAlert(title: "Error", message: "Enter valid Email address")
            
            return
        }
        
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        let parameters = ["user_email": emailAddress]
        UserServices.ForgotPassword(param: parameters, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
//            print(response!)
            
            let msg = response?["message"].stringValue
            print("Message: \(String(describing: msg!))")
            
            Alert.showAlert(title: "Success", message: msg!)
            
        })
    }
    
}

//MARK:-  UITextFieldDelegate

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        return true
    }
}

