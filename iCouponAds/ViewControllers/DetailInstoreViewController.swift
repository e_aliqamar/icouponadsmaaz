//
//  DetailInstoreViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 18/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class DetailInstoreViewController: UIViewController {

    class func instantiateFromStoryboard() -> DetailInstoreViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DetailInstoreViewController
    }
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblNoOfDeals: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblUrl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var detailInStoreDeals: InStore!
    let cellIdentifier = "MyCardsTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        backButton()
        
        self.title = detailInStoreDeals.businessName!
//        self.imgBanner.setImageFromUrl(urlStr: detailInStoreDeals.displayImage!)
        self.imgLogo.setImageFromUrl(urlStr: detailInStoreDeals.logoImage!)
        self.lblUrl.text = detailInStoreDeals.businessWebsite!
        self.txtDescription.text = detailInStoreDeals.businessDescription!
        if detailInStoreDeals.noOfDeals! > 1 {
            self.lblNoOfDeals.text = "\(detailInStoreDeals.noOfDeals!) ACTIVE DEALS"
        } else {
            self.lblNoOfDeals.text = "\(detailInStoreDeals.noOfDeals!) ACTIVE DEAL"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
}
