//
//  GetCartViewController.swift
//  iCouponAds
//
//  Created by Ali Qamar on 16/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class GetCartViewController: UIViewController {

    class func instantiateFromStoryboard() -> GetCartViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! GetCartViewController
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lblTotal: UILabel!
    
    // MARK: - Properties
    
    let cellIdentifier = "CartTableViewCell"
    var cartArray: [Cart]? = [Cart]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.resetNavigationBar()
        self.navigationController?.setNavigationBarTitleColor(color: UIColor.white)
        self.title = "Cart"
        
        backButton()
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
        
        self.getCartListCoupon()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:-  Navigation Bar Button Code
    
    func backButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage(named: "Back"), for: .normal)
        leftBtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func sum() {
        var sum = 0.0
        for item in self.cartArray! {
            sum = sum + Double(item.totalAmount!)!
        }
        
        DispatchQueue.main.async {
            self.lblTotal.text = "\(sum)"
        }
    }
    
    @IBAction func checkoutAction(_ sender: UIButton) {
        let vc = CheckoutViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension GetCartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.cartArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CartTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CartTableViewCell
        
        let obj = self.cartArray![indexPath.row]
        
        cell.lblAmount.text = "$\(obj.totalAmount!)"
        cell.lblTitle.text = obj.itemName!
        
        cell.imgBanner.setImageFromUrl(urlStr: obj.itemImage!)
        
        cell.indexPath = indexPath
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
    }
}

//MARK:-  UIButtonCellDelegate

extension GetCartViewController: ButtonClickDelegate {
    func click(indexPath: IndexPath) {
        self.removeFromCart(indexPath: indexPath)
    }
}

//MARK:-  Service Methods

extension GetCartViewController {
    
    func getCartListCoupon() {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        let parameters = ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!]
        CouponServices.GetCart(param: parameters, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)

            let cartResponseArray = response?["data"].arrayValue
            //            print(couponsResponseArray!)
            for resultObj in cartResponseArray! {
                let obj = Cart(json: resultObj)
                self.cartArray?.append(obj)
            }
            
            self.tableView.reloadData()
            
            self.sum()
        })
    }
    
    func removeFromCart(indexPath: IndexPath) {
        SKActivityIndicator.show("", userInteractionStatus: false)
        
        let obj = self.cartArray![indexPath.row]
        
        let parameter = ["t_user_id": (Singleton.sharedInstance.CurrentUser?.userId!)!,
                         "cartId": obj.cartId!]
        
        CouponServices.RemoveFromCart(param: parameter, completionHandler: {(status, response, error) in
            
            SKActivityIndicator.dismiss()
            
            if !status {
                if error != nil {
                    let msg = (error as! Error).localizedDescription
                    print("Error: \(msg)")
                    
                    Alert.showAlert(title: "Error", message: msg)
                    
                    return
                }
                let msg = response?["message"].stringValue
                print("Message: \(String(describing: msg!))")
                
                Alert.showAlert(title: "Error", message: msg!)
                
                return
            }
            
            print("SUCCESS")
            //            print(response!)
            
            self.cartArray?.remove(at: indexPath.row)
            self.tableView.reloadData()
            
            self.sum()
            
            let msg = response?["message"].stringValue
            Alert.showAlert(title: "Success", message: msg!)
        })
    }
}
