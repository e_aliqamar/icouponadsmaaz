//
//  Clipping.swift
//
//  Created by Ali Qamar on 05/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Clipping: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let isExpired = "isExpired"
        static let name = "name"
        static let id = "id"
        static let image = "image"
        static let sortName = "sortName"
        static let type = "type"
        static let flyerUrl = "flyer_url"
    }
    
    // MARK: Properties
    public var isExpired: String?
    public var name: String?
    public var id: String?
    public var image: String?
    public var sortName: String?
    public var type: String?
    public var flyerUrl: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        isExpired = json[SerializationKeys.isExpired].string
        name = json[SerializationKeys.name].string
        id = json[SerializationKeys.id].string
        image = json[SerializationKeys.image].string
        sortName = json[SerializationKeys.sortName].string
        type = json[SerializationKeys.type].string
        flyerUrl = json[SerializationKeys.flyerUrl].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = isExpired { dictionary[SerializationKeys.isExpired] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = sortName { dictionary[SerializationKeys.sortName] = value }
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = flyerUrl { dictionary[SerializationKeys.flyerUrl] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.isExpired = aDecoder.decodeObject(forKey: SerializationKeys.isExpired) as? String
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
        self.sortName = aDecoder.decodeObject(forKey: SerializationKeys.sortName) as? String
        self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
        self.flyerUrl = aDecoder.decodeObject(forKey: SerializationKeys.flyerUrl) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(isExpired, forKey: SerializationKeys.isExpired)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(image, forKey: SerializationKeys.image)
        aCoder.encode(sortName, forKey: SerializationKeys.sortName)
        aCoder.encode(type, forKey: SerializationKeys.type)
        aCoder.encode(flyerUrl, forKey: SerializationKeys.flyerUrl)
    }
    
}

