//
//  InStore.swift
//
//  Created by Ali Qamar on 17/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class InStore: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let isSave = "isSave"
    static let noOfDeals = "noOfDeals"
    static let businessClosedDay = "businessClosedDay"
    static let businessOpenDay = "businessOpenDay"
    static let businessLong = "businessLong"
    static let tBusinessId = "t_business_id"
    static let businessLat = "businessLat"
    static let businessName = "businessName"
    static let businessAddress = "businessAddress"
    static let logoImage = "logoImage"
    static let businessZipcode = "businessZipcode"
    static let businessVideo = "businessVideo"
    static let businessWebsite = "businessWebsite"
    static let businessClosedTime = "businessClosedTime"
    static let businessOpenTime = "businessOpenTime"
    static let businessDescription = "businessDescription"
  }

  // MARK: Properties
  public var isSave: Int?
  public var noOfDeals: Int?
  public var businessClosedDay: String?
  public var businessOpenDay: String?
  public var businessLong: String?
  public var tBusinessId: String?
  public var businessLat: String?
  public var businessName: String?
  public var businessAddress: String?
  public var logoImage: String?
  public var businessZipcode: String?
  public var businessVideo: String?
  public var businessWebsite: String?
  public var businessClosedTime: String?
  public var businessOpenTime: String?
  public var businessDescription: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    isSave = json[SerializationKeys.isSave].int
    noOfDeals = json[SerializationKeys.noOfDeals].int
    businessClosedDay = json[SerializationKeys.businessClosedDay].string
    businessOpenDay = json[SerializationKeys.businessOpenDay].string
    businessLong = json[SerializationKeys.businessLong].string
    tBusinessId = json[SerializationKeys.tBusinessId].string
    businessLat = json[SerializationKeys.businessLat].string
    businessName = json[SerializationKeys.businessName].string
    businessAddress = json[SerializationKeys.businessAddress].string
    logoImage = json[SerializationKeys.logoImage].string
    businessZipcode = json[SerializationKeys.businessZipcode].string
    businessVideo = json[SerializationKeys.businessVideo].string
    businessWebsite = json[SerializationKeys.businessWebsite].string
    businessClosedTime = json[SerializationKeys.businessClosedTime].string
    businessOpenTime = json[SerializationKeys.businessOpenTime].string
    businessDescription = json[SerializationKeys.businessDescription].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = isSave { dictionary[SerializationKeys.isSave] = value }
    if let value = noOfDeals { dictionary[SerializationKeys.noOfDeals] = value }
    if let value = businessClosedDay { dictionary[SerializationKeys.businessClosedDay] = value }
    if let value = businessOpenDay { dictionary[SerializationKeys.businessOpenDay] = value }
    if let value = businessLong { dictionary[SerializationKeys.businessLong] = value }
    if let value = tBusinessId { dictionary[SerializationKeys.tBusinessId] = value }
    if let value = businessLat { dictionary[SerializationKeys.businessLat] = value }
    if let value = businessName { dictionary[SerializationKeys.businessName] = value }
    if let value = businessAddress { dictionary[SerializationKeys.businessAddress] = value }
    if let value = logoImage { dictionary[SerializationKeys.logoImage] = value }
    if let value = businessZipcode { dictionary[SerializationKeys.businessZipcode] = value }
    if let value = businessVideo { dictionary[SerializationKeys.businessVideo] = value }
    if let value = businessWebsite { dictionary[SerializationKeys.businessWebsite] = value }
    if let value = businessClosedTime { dictionary[SerializationKeys.businessClosedTime] = value }
    if let value = businessOpenTime { dictionary[SerializationKeys.businessOpenTime] = value }
    if let value = businessDescription { dictionary[SerializationKeys.businessDescription] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.isSave = aDecoder.decodeObject(forKey: SerializationKeys.isSave) as? Int
    self.noOfDeals = aDecoder.decodeObject(forKey: SerializationKeys.noOfDeals) as? Int
    self.businessClosedDay = aDecoder.decodeObject(forKey: SerializationKeys.businessClosedDay) as? String
    self.businessOpenDay = aDecoder.decodeObject(forKey: SerializationKeys.businessOpenDay) as? String
    self.businessLong = aDecoder.decodeObject(forKey: SerializationKeys.businessLong) as? String
    self.tBusinessId = aDecoder.decodeObject(forKey: SerializationKeys.tBusinessId) as? String
    self.businessLat = aDecoder.decodeObject(forKey: SerializationKeys.businessLat) as? String
    self.businessName = aDecoder.decodeObject(forKey: SerializationKeys.businessName) as? String
    self.businessAddress = aDecoder.decodeObject(forKey: SerializationKeys.businessAddress) as? String
    self.logoImage = aDecoder.decodeObject(forKey: SerializationKeys.logoImage) as? String
    self.businessZipcode = aDecoder.decodeObject(forKey: SerializationKeys.businessZipcode) as? String
    self.businessVideo = aDecoder.decodeObject(forKey: SerializationKeys.businessVideo) as? String
    self.businessWebsite = aDecoder.decodeObject(forKey: SerializationKeys.businessWebsite) as? String
    self.businessClosedTime = aDecoder.decodeObject(forKey: SerializationKeys.businessClosedTime) as? String
    self.businessOpenTime = aDecoder.decodeObject(forKey: SerializationKeys.businessOpenTime) as? String
    self.businessDescription = aDecoder.decodeObject(forKey: SerializationKeys.businessDescription) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(isSave, forKey: SerializationKeys.isSave)
    aCoder.encode(noOfDeals, forKey: SerializationKeys.noOfDeals)
    aCoder.encode(businessClosedDay, forKey: SerializationKeys.businessClosedDay)
    aCoder.encode(businessOpenDay, forKey: SerializationKeys.businessOpenDay)
    aCoder.encode(businessLong, forKey: SerializationKeys.businessLong)
    aCoder.encode(tBusinessId, forKey: SerializationKeys.tBusinessId)
    aCoder.encode(businessLat, forKey: SerializationKeys.businessLat)
    aCoder.encode(businessName, forKey: SerializationKeys.businessName)
    aCoder.encode(businessAddress, forKey: SerializationKeys.businessAddress)
    aCoder.encode(logoImage, forKey: SerializationKeys.logoImage)
    aCoder.encode(businessZipcode, forKey: SerializationKeys.businessZipcode)
    aCoder.encode(businessVideo, forKey: SerializationKeys.businessVideo)
    aCoder.encode(businessWebsite, forKey: SerializationKeys.businessWebsite)
    aCoder.encode(businessClosedTime, forKey: SerializationKeys.businessClosedTime)
    aCoder.encode(businessOpenTime, forKey: SerializationKeys.businessOpenTime)
    aCoder.encode(businessDescription, forKey: SerializationKeys.businessDescription)
  }

}
