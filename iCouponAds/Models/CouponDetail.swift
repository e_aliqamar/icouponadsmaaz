//
//  CouponDetail.swift
//
//  Created by Ali Qamar on 13/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CouponDetail: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bDesc = "bDesc"
    static let businessLogo = "businessLogo"
    static let name = "name"
    static let image = "image"
    static let totalAmount = "totalAmount"
    static let idCategory = "idCategory"
    static let descriptionValue = "description"
    static let saveAmount = "saveAmount"
    static let isInCart = "isInCart"
    static let validDate = "validDate"
  }

  // MARK: Properties
  public var bDesc: String?
  public var businessLogo: String?
  public var name: String?
  public var image: String?
  public var totalAmount: String?
  public var idCategory: String?
  public var descriptionValue: String?
  public var saveAmount: String?
  public var isInCart: String?
  public var validDate: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: JSON) {
    self.init(json: object)
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    bDesc = json[SerializationKeys.bDesc].string
    businessLogo = json[SerializationKeys.businessLogo].string
    name = json[SerializationKeys.name].string
    image = json[SerializationKeys.image].string
    totalAmount = json[SerializationKeys.totalAmount].string
    idCategory = json[SerializationKeys.idCategory].string
    descriptionValue = json[SerializationKeys.descriptionValue].string
    saveAmount = json[SerializationKeys.saveAmount].string
    isInCart = json[SerializationKeys.isInCart].string
    validDate = json[SerializationKeys.validDate].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = bDesc { dictionary[SerializationKeys.bDesc] = value }
    if let value = businessLogo { dictionary[SerializationKeys.businessLogo] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = totalAmount { dictionary[SerializationKeys.totalAmount] = value }
    if let value = idCategory { dictionary[SerializationKeys.idCategory] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = saveAmount { dictionary[SerializationKeys.saveAmount] = value }
    if let value = isInCart { dictionary[SerializationKeys.isInCart] = value }
    if let value = validDate { dictionary[SerializationKeys.validDate] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.bDesc = aDecoder.decodeObject(forKey: SerializationKeys.bDesc) as? String
    self.businessLogo = aDecoder.decodeObject(forKey: SerializationKeys.businessLogo) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.totalAmount = aDecoder.decodeObject(forKey: SerializationKeys.totalAmount) as? String
    self.idCategory = aDecoder.decodeObject(forKey: SerializationKeys.idCategory) as? String
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.saveAmount = aDecoder.decodeObject(forKey: SerializationKeys.saveAmount) as? String
    self.isInCart = aDecoder.decodeObject(forKey: SerializationKeys.isInCart) as? String
    self.validDate = aDecoder.decodeObject(forKey: SerializationKeys.validDate) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(bDesc, forKey: SerializationKeys.bDesc)
    aCoder.encode(businessLogo, forKey: SerializationKeys.businessLogo)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(totalAmount, forKey: SerializationKeys.totalAmount)
    aCoder.encode(idCategory, forKey: SerializationKeys.idCategory)
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(saveAmount, forKey: SerializationKeys.saveAmount)
    aCoder.encode(isInCart, forKey: SerializationKeys.isInCart)
    aCoder.encode(validDate, forKey: SerializationKeys.validDate)
  }

}
