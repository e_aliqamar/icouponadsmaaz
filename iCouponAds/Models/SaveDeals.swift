//
//  SaveDeals.swift
//
//  Created by Ali Qamar on 17/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SaveDeals: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let dealPrice = "dealPrice"
    static let discountTitle = "discountTitle"
    static let businessClosedDay = "businessClosedDay"
    static let dealDescription = "dealDescription"
    static let expiryDate = "expiryDate"
    static let displayImage = "displayImage"
    static let dealName = "dealName"
    static let businessOpenDay = "businessOpenDay"
    static let userName = "userName"
    static let businessLong = "businessLong"
    static let businessLat = "businessLat"
    static let businessName = "businessName"
    static let tUserId = "t_user_id"
    static let businessAddress = "businessAddress"
    static let dealVideo = "dealVideo"
    static let tDealId = "t_deal_id"
    static let cashBack = "cashBack"
    static let dateAdded = "date_added"
    static let userEmail = "userEmail"
    static let dealWebsite = "dealWebsite"
    static let businessClosedTime = "businessClosedTime"
    static let businessOpenTime = "businessOpenTime"
  }

  // MARK: Properties
  public var dealPrice: String?
  public var discountTitle: String?
  public var businessClosedDay: String?
  public var dealDescription: String?
  public var expiryDate: String?
  public var displayImage: String?
  public var dealName: String?
  public var businessOpenDay: String?
  public var userName: String?
  public var businessLong: String?
  public var businessLat: String?
  public var businessName: String?
  public var tUserId: String?
  public var businessAddress: String?
  public var dealVideo: String?
  public var tDealId: String?
  public var cashBack: String?
  public var dateAdded: String?
  public var userEmail: String?
  public var dealWebsite: String?
  public var businessClosedTime: String?
  public var businessOpenTime: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: JSON) {
    self.init(json: object)
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    dealPrice = json[SerializationKeys.dealPrice].string
    discountTitle = json[SerializationKeys.discountTitle].string
    businessClosedDay = json[SerializationKeys.businessClosedDay].string
    dealDescription = json[SerializationKeys.dealDescription].string
    expiryDate = json[SerializationKeys.expiryDate].string
    displayImage = json[SerializationKeys.displayImage].string
    dealName = json[SerializationKeys.dealName].string
    businessOpenDay = json[SerializationKeys.businessOpenDay].string
    userName = json[SerializationKeys.userName].string
    businessLong = json[SerializationKeys.businessLong].string
    businessLat = json[SerializationKeys.businessLat].string
    businessName = json[SerializationKeys.businessName].string
    tUserId = json[SerializationKeys.tUserId].string
    businessAddress = json[SerializationKeys.businessAddress].string
    dealVideo = json[SerializationKeys.dealVideo].string
    tDealId = json[SerializationKeys.tDealId].string
    cashBack = json[SerializationKeys.cashBack].string
    dateAdded = json[SerializationKeys.dateAdded].string
    userEmail = json[SerializationKeys.userEmail].string
    dealWebsite = json[SerializationKeys.dealWebsite].string
    businessClosedTime = json[SerializationKeys.businessClosedTime].string
    businessOpenTime = json[SerializationKeys.businessOpenTime].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = dealPrice { dictionary[SerializationKeys.dealPrice] = value }
    if let value = discountTitle { dictionary[SerializationKeys.discountTitle] = value }
    if let value = businessClosedDay { dictionary[SerializationKeys.businessClosedDay] = value }
    if let value = dealDescription { dictionary[SerializationKeys.dealDescription] = value }
    if let value = expiryDate { dictionary[SerializationKeys.expiryDate] = value }
    if let value = displayImage { dictionary[SerializationKeys.displayImage] = value }
    if let value = dealName { dictionary[SerializationKeys.dealName] = value }
    if let value = businessOpenDay { dictionary[SerializationKeys.businessOpenDay] = value }
    if let value = userName { dictionary[SerializationKeys.userName] = value }
    if let value = businessLong { dictionary[SerializationKeys.businessLong] = value }
    if let value = businessLat { dictionary[SerializationKeys.businessLat] = value }
    if let value = businessName { dictionary[SerializationKeys.businessName] = value }
    if let value = tUserId { dictionary[SerializationKeys.tUserId] = value }
    if let value = businessAddress { dictionary[SerializationKeys.businessAddress] = value }
    if let value = dealVideo { dictionary[SerializationKeys.dealVideo] = value }
    if let value = tDealId { dictionary[SerializationKeys.tDealId] = value }
    if let value = cashBack { dictionary[SerializationKeys.cashBack] = value }
    if let value = dateAdded { dictionary[SerializationKeys.dateAdded] = value }
    if let value = userEmail { dictionary[SerializationKeys.userEmail] = value }
    if let value = dealWebsite { dictionary[SerializationKeys.dealWebsite] = value }
    if let value = businessClosedTime { dictionary[SerializationKeys.businessClosedTime] = value }
    if let value = businessOpenTime { dictionary[SerializationKeys.businessOpenTime] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.dealPrice = aDecoder.decodeObject(forKey: SerializationKeys.dealPrice) as? String
    self.discountTitle = aDecoder.decodeObject(forKey: SerializationKeys.discountTitle) as? String
    self.businessClosedDay = aDecoder.decodeObject(forKey: SerializationKeys.businessClosedDay) as? String
    self.dealDescription = aDecoder.decodeObject(forKey: SerializationKeys.dealDescription) as? String
    self.expiryDate = aDecoder.decodeObject(forKey: SerializationKeys.expiryDate) as? String
    self.displayImage = aDecoder.decodeObject(forKey: SerializationKeys.displayImage) as? String
    self.dealName = aDecoder.decodeObject(forKey: SerializationKeys.dealName) as? String
    self.businessOpenDay = aDecoder.decodeObject(forKey: SerializationKeys.businessOpenDay) as? String
    self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
    self.businessLong = aDecoder.decodeObject(forKey: SerializationKeys.businessLong) as? String
    self.businessLat = aDecoder.decodeObject(forKey: SerializationKeys.businessLat) as? String
    self.businessName = aDecoder.decodeObject(forKey: SerializationKeys.businessName) as? String
    self.tUserId = aDecoder.decodeObject(forKey: SerializationKeys.tUserId) as? String
    self.businessAddress = aDecoder.decodeObject(forKey: SerializationKeys.businessAddress) as? String
    self.dealVideo = aDecoder.decodeObject(forKey: SerializationKeys.dealVideo) as? String
    self.tDealId = aDecoder.decodeObject(forKey: SerializationKeys.tDealId) as? String
    self.cashBack = aDecoder.decodeObject(forKey: SerializationKeys.cashBack) as? String
    self.dateAdded = aDecoder.decodeObject(forKey: SerializationKeys.dateAdded) as? String
    self.userEmail = aDecoder.decodeObject(forKey: SerializationKeys.userEmail) as? String
    self.dealWebsite = aDecoder.decodeObject(forKey: SerializationKeys.dealWebsite) as? String
    self.businessClosedTime = aDecoder.decodeObject(forKey: SerializationKeys.businessClosedTime) as? String
    self.businessOpenTime = aDecoder.decodeObject(forKey: SerializationKeys.businessOpenTime) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(dealPrice, forKey: SerializationKeys.dealPrice)
    aCoder.encode(discountTitle, forKey: SerializationKeys.discountTitle)
    aCoder.encode(businessClosedDay, forKey: SerializationKeys.businessClosedDay)
    aCoder.encode(dealDescription, forKey: SerializationKeys.dealDescription)
    aCoder.encode(expiryDate, forKey: SerializationKeys.expiryDate)
    aCoder.encode(displayImage, forKey: SerializationKeys.displayImage)
    aCoder.encode(dealName, forKey: SerializationKeys.dealName)
    aCoder.encode(businessOpenDay, forKey: SerializationKeys.businessOpenDay)
    aCoder.encode(userName, forKey: SerializationKeys.userName)
    aCoder.encode(businessLong, forKey: SerializationKeys.businessLong)
    aCoder.encode(businessLat, forKey: SerializationKeys.businessLat)
    aCoder.encode(businessName, forKey: SerializationKeys.businessName)
    aCoder.encode(tUserId, forKey: SerializationKeys.tUserId)
    aCoder.encode(businessAddress, forKey: SerializationKeys.businessAddress)
    aCoder.encode(dealVideo, forKey: SerializationKeys.dealVideo)
    aCoder.encode(tDealId, forKey: SerializationKeys.tDealId)
    aCoder.encode(cashBack, forKey: SerializationKeys.cashBack)
    aCoder.encode(dateAdded, forKey: SerializationKeys.dateAdded)
    aCoder.encode(userEmail, forKey: SerializationKeys.userEmail)
    aCoder.encode(dealWebsite, forKey: SerializationKeys.dealWebsite)
    aCoder.encode(businessClosedTime, forKey: SerializationKeys.businessClosedTime)
    aCoder.encode(businessOpenTime, forKey: SerializationKeys.businessOpenTime)
  }

}
