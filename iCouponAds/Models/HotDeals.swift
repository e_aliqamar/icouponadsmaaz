//
//  HotDeals.swift
//
//  Created by Ali Qamar on 05/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class HotDeals: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let dealPrice = "dealPrice"
        static let discountTitle = "discountTitle"
        static let isSave = "isSave"
        static let dealDescription = "dealDescription"
        static let expiryDate = "expiryDate"
        static let displayImage = "displayImage"
        static let dealName = "dealName"
        static let businessLong = "businessLong"
        static let tBusinessId = "t_business_id"
        static let businessLat = "businessLat"
        static let businessName = "businessName"
        static let dealVideo = "dealVideo"
        static let logoImage = "logoImage"
        static let cashBack = "cashBack"
        static let tDealId = "t_deal_id"
        static let businessOpenDay = "businessOpenDay"
        static let businessClosedDay = "businessClosedDay"
        static let businessOpenTime = "businessOpenTime"
        static let businessClosedTime = "businessClosedTime"
        static let businessAddress = "businessAddress"
        static let businessZipcode = "businessZipcode"
        static let businessWebsite = "businessWebsite"
        static let businessVideo = "businessVideo"
        static let dealWebsite = "dealWebsite"
        static let businessDescription = "businessDescription"
    }
    
    // MARK: Properties
    public var dealPrice: String?
    public var discountTitle: String?
    public var isSave: Int?
    public var dealDescription: String?
    public var expiryDate: String?
    public var displayImage: String?
    public var dealName: String?
    public var businessLong: String?
    public var tBusinessId: String?
    public var businessLat: String?
    public var businessName: String?
    public var dealVideo: String?
    public var logoImage: String?
    public var cashBack: String?
    public var tDealId: String?
    public var businessOpenDay: String?
    public var businessClosedDay: String?
    public var businessOpenTime: String?
    public var businessClosedTime: String?
    public var businessAddress: String?
    public var businessZipcode: String?
    public var businessWebsite: String?
    public var businessVideo: String?
    public var dealWebsite: String?
    public var businessDescription: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        dealPrice = json[SerializationKeys.dealPrice].string
        discountTitle = json[SerializationKeys.discountTitle].string
        isSave = json[SerializationKeys.isSave].int
        dealDescription = json[SerializationKeys.dealDescription].string
        expiryDate = json[SerializationKeys.expiryDate].string
        displayImage = json[SerializationKeys.displayImage].string
        dealName = json[SerializationKeys.dealName].string
        businessLong = json[SerializationKeys.businessLong].string
        tBusinessId = json[SerializationKeys.tBusinessId].string
        businessLat = json[SerializationKeys.businessLat].string
        businessName = json[SerializationKeys.businessName].string
        dealVideo = json[SerializationKeys.dealVideo].string
        logoImage = json[SerializationKeys.logoImage].string
        cashBack = json[SerializationKeys.cashBack].string
        tDealId = json[SerializationKeys.tDealId].string
        businessOpenDay = json[SerializationKeys.businessOpenDay].string
        businessClosedDay = json[SerializationKeys.businessClosedDay].string
        businessOpenTime = json[SerializationKeys.businessOpenTime].string
        businessClosedTime = json[SerializationKeys.businessClosedTime].string
        businessAddress = json[SerializationKeys.businessAddress].string
        businessZipcode = json[SerializationKeys.businessZipcode].string
        businessWebsite = json[SerializationKeys.businessWebsite].string
        businessVideo = json[SerializationKeys.businessVideo].string
        dealWebsite = json[SerializationKeys.dealWebsite].string
        businessDescription = json[SerializationKeys.businessDescription].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = dealPrice { dictionary[SerializationKeys.dealPrice] = value }
        if let value = discountTitle { dictionary[SerializationKeys.discountTitle] = value }
        if let value = isSave { dictionary[SerializationKeys.isSave] = value }
        if let value = dealDescription { dictionary[SerializationKeys.dealDescription] = value }
        if let value = expiryDate { dictionary[SerializationKeys.expiryDate] = value }
        if let value = displayImage { dictionary[SerializationKeys.displayImage] = value }
        if let value = dealName { dictionary[SerializationKeys.dealName] = value }
        if let value = businessLong { dictionary[SerializationKeys.businessLong] = value }
        if let value = tBusinessId { dictionary[SerializationKeys.tBusinessId] = value }
        if let value = businessLat { dictionary[SerializationKeys.businessLat] = value }
        if let value = businessName { dictionary[SerializationKeys.businessName] = value }
        if let value = dealVideo { dictionary[SerializationKeys.dealVideo] = value }
        if let value = logoImage { dictionary[SerializationKeys.logoImage] = value }
        if let value = cashBack { dictionary[SerializationKeys.cashBack] = value }
        if let value = tDealId { dictionary[SerializationKeys.tDealId] = value }
        if let value = businessOpenDay { dictionary[SerializationKeys.businessOpenDay] = value }
        if let value = businessClosedDay { dictionary[SerializationKeys.businessClosedDay] = value }
        if let value = businessOpenTime { dictionary[SerializationKeys.businessOpenTime] = value }
        if let value = businessClosedTime { dictionary[SerializationKeys.businessClosedTime] = value }
        if let value = businessAddress { dictionary[SerializationKeys.businessAddress] = value }
        if let value = businessZipcode { dictionary[SerializationKeys.businessZipcode] = value }
        if let value = businessWebsite { dictionary[SerializationKeys.businessWebsite] = value }
        if let value = businessVideo { dictionary[SerializationKeys.businessVideo] = value }
        if let value = dealWebsite { dictionary[SerializationKeys.dealWebsite] = value }
        if let value = businessDescription { dictionary[SerializationKeys.businessDescription] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.dealPrice = aDecoder.decodeObject(forKey: SerializationKeys.dealPrice) as? String
        self.discountTitle = aDecoder.decodeObject(forKey: SerializationKeys.discountTitle) as? String
        self.isSave = aDecoder.decodeObject(forKey: SerializationKeys.isSave) as? Int
        self.dealDescription = aDecoder.decodeObject(forKey: SerializationKeys.dealDescription) as? String
        self.expiryDate = aDecoder.decodeObject(forKey: SerializationKeys.expiryDate) as? String
        self.displayImage = aDecoder.decodeObject(forKey: SerializationKeys.displayImage) as? String
        self.dealName = aDecoder.decodeObject(forKey: SerializationKeys.dealName) as? String
        self.businessLong = aDecoder.decodeObject(forKey: SerializationKeys.businessLong) as? String
        self.tBusinessId = aDecoder.decodeObject(forKey: SerializationKeys.tBusinessId) as? String
        self.businessLat = aDecoder.decodeObject(forKey: SerializationKeys.businessLat) as? String
        self.businessName = aDecoder.decodeObject(forKey: SerializationKeys.businessName) as? String
        self.dealVideo = aDecoder.decodeObject(forKey: SerializationKeys.dealVideo) as? String
        self.logoImage = aDecoder.decodeObject(forKey: SerializationKeys.logoImage) as? String
        self.cashBack = aDecoder.decodeObject(forKey: SerializationKeys.cashBack) as? String
        self.tDealId = aDecoder.decodeObject(forKey: SerializationKeys.tDealId) as? String
        self.businessOpenDay = aDecoder.decodeObject(forKey: SerializationKeys.businessOpenDay) as? String
        self.businessClosedDay = aDecoder.decodeObject(forKey: SerializationKeys.businessClosedDay) as? String
        self.businessOpenTime = aDecoder.decodeObject(forKey: SerializationKeys.businessOpenTime) as? String
        self.businessClosedTime = aDecoder.decodeObject(forKey: SerializationKeys.businessClosedTime) as? String
        self.businessAddress = aDecoder.decodeObject(forKey: SerializationKeys.businessAddress) as? String
        self.businessZipcode = aDecoder.decodeObject(forKey: SerializationKeys.businessZipcode) as? String
        self.businessWebsite = aDecoder.decodeObject(forKey: SerializationKeys.businessWebsite) as? String
        self.businessVideo = aDecoder.decodeObject(forKey: SerializationKeys.businessVideo) as? String
        self.dealWebsite = aDecoder.decodeObject(forKey: SerializationKeys.dealWebsite) as? String
        self.businessDescription = aDecoder.decodeObject(forKey: SerializationKeys.businessDescription) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(dealPrice, forKey: SerializationKeys.dealPrice)
        aCoder.encode(discountTitle, forKey: SerializationKeys.discountTitle)
        aCoder.encode(isSave, forKey: SerializationKeys.isSave)
        aCoder.encode(dealDescription, forKey: SerializationKeys.dealDescription)
        aCoder.encode(expiryDate, forKey: SerializationKeys.expiryDate)
        aCoder.encode(displayImage, forKey: SerializationKeys.displayImage)
        aCoder.encode(dealName, forKey: SerializationKeys.dealName)
        aCoder.encode(businessLong, forKey: SerializationKeys.businessLong)
        aCoder.encode(tBusinessId, forKey: SerializationKeys.tBusinessId)
        aCoder.encode(businessLat, forKey: SerializationKeys.businessLat)
        aCoder.encode(businessName, forKey: SerializationKeys.businessName)
        aCoder.encode(dealVideo, forKey: SerializationKeys.dealVideo)
        aCoder.encode(logoImage, forKey: SerializationKeys.logoImage)
        aCoder.encode(cashBack, forKey: SerializationKeys.cashBack)
        aCoder.encode(tDealId, forKey: SerializationKeys.tDealId)
        aCoder.encode(businessOpenDay, forKey: SerializationKeys.businessOpenDay)
        aCoder.encode(businessClosedDay, forKey: SerializationKeys.businessClosedDay)
        aCoder.encode(businessOpenTime, forKey: SerializationKeys.businessOpenTime)
        aCoder.encode(businessClosedTime, forKey: SerializationKeys.businessClosedTime)
        aCoder.encode(businessAddress, forKey: SerializationKeys.businessAddress)
        aCoder.encode(businessZipcode, forKey: SerializationKeys.businessZipcode)
        aCoder.encode(businessWebsite, forKey: SerializationKeys.businessWebsite)
        aCoder.encode(businessVideo, forKey: SerializationKeys.businessVideo)
        aCoder.encode(dealWebsite, forKey: SerializationKeys.dealWebsite)
        aCoder.encode(businessDescription, forKey: SerializationKeys.businessDescription)
    }
    
}

