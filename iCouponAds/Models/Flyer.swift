//
//  Flyer.swift
//
//  Created by Ali Qamar on 04/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Flyer: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let flyer = "flyer"
        static let link = "link"
        static let businessLogo = "businessLogo"
        static let isNew = "isNew"
        static let dateAdded = "date_added"
        static let image = "image"
        static let expiryDate = "expiryDate"
        static let clipLink = "clipLink"
        static let descriptionValue = "description"
        static let tFlyerId = "t_flyer_id"
        static let isFavourite = "isFavourite"
        static let isFeatured = "isFeatured"
    }
    
    // MARK: Properties
    public var flyer: String?
    public var link: String?
    public var businessLogo: String?
    public var isNew: String?
    public var dateAdded: String?
    public var image: String?
    public var expiryDate: String?
    public var clipLink: String?
    public var descriptionValue: String?
    public var tFlyerId: String?
    public var isFavourite: Int?
    public var isFeatured: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        flyer = json[SerializationKeys.flyer].string
        link = json[SerializationKeys.link].string
        businessLogo = json[SerializationKeys.businessLogo].string
        isNew = json[SerializationKeys.isNew].string
        dateAdded = json[SerializationKeys.dateAdded].string
        image = json[SerializationKeys.image].string
        expiryDate = json[SerializationKeys.expiryDate].string
        clipLink = json[SerializationKeys.clipLink].string
        descriptionValue = json[SerializationKeys.descriptionValue].string
        tFlyerId = json[SerializationKeys.tFlyerId].string
        isFavourite = json[SerializationKeys.isFavourite].int
        isFeatured = json[SerializationKeys.isFeatured].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = flyer { dictionary[SerializationKeys.flyer] = value }
        if let value = link { dictionary[SerializationKeys.link] = value }
        if let value = businessLogo { dictionary[SerializationKeys.businessLogo] = value }
        if let value = isNew { dictionary[SerializationKeys.isNew] = value }
        if let value = dateAdded { dictionary[SerializationKeys.dateAdded] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = expiryDate { dictionary[SerializationKeys.expiryDate] = value }
        if let value = clipLink { dictionary[SerializationKeys.clipLink] = value }
        if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
        if let value = tFlyerId { dictionary[SerializationKeys.tFlyerId] = value }
        if let value = isFavourite { dictionary[SerializationKeys.isFavourite] = value }
        if let value = isFeatured { dictionary[SerializationKeys.isFeatured] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.flyer = aDecoder.decodeObject(forKey: SerializationKeys.flyer) as? String
        self.link = aDecoder.decodeObject(forKey: SerializationKeys.link) as? String
        self.businessLogo = aDecoder.decodeObject(forKey: SerializationKeys.businessLogo) as? String
        self.isNew = aDecoder.decodeObject(forKey: SerializationKeys.isNew) as? String
        self.dateAdded = aDecoder.decodeObject(forKey: SerializationKeys.dateAdded) as? String
        self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
        self.expiryDate = aDecoder.decodeObject(forKey: SerializationKeys.expiryDate) as? String
        self.clipLink = aDecoder.decodeObject(forKey: SerializationKeys.clipLink) as? String
        self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
        self.tFlyerId = aDecoder.decodeObject(forKey: SerializationKeys.tFlyerId) as? String
        self.isFavourite = aDecoder.decodeObject(forKey: SerializationKeys.isFavourite) as? Int
        self.isFeatured = aDecoder.decodeObject(forKey: SerializationKeys.isFeatured) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(flyer, forKey: SerializationKeys.flyer)
        aCoder.encode(link, forKey: SerializationKeys.link)
        aCoder.encode(businessLogo, forKey: SerializationKeys.businessLogo)
        aCoder.encode(isNew, forKey: SerializationKeys.isNew)
        aCoder.encode(dateAdded, forKey: SerializationKeys.dateAdded)
        aCoder.encode(image, forKey: SerializationKeys.image)
        aCoder.encode(expiryDate, forKey: SerializationKeys.expiryDate)
        aCoder.encode(clipLink, forKey: SerializationKeys.clipLink)
        aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
        aCoder.encode(tFlyerId, forKey: SerializationKeys.tFlyerId)
        aCoder.encode(isFavourite, forKey: SerializationKeys.isFavourite)
        aCoder.encode(isFeatured, forKey: SerializationKeys.isFeatured)
    }
    
}


