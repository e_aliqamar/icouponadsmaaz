//
//  Coupon.swift
//
//  Created by Ali Qamar on 04/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Coupon: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let coupan = "coupan"
        static let businessLogo = "businessLogo"
        static let coupanType = "coupanType"
        static let tCoupanId = "t_coupan_id"
        static let save = "save"
        static let totalAmount = "totalAmount"
        static let expiryDate = "expiryDate"
        static let descriptionValue = "description"
        static let saveAmount = "saveAmount"
        static let currentAmount = "currentAmount"
        static let imglink = "imglink"
        static let dateAdded = "dateAdded"
    }
    
    // MARK: Properties
    public var coupan: String?
    public var businessLogo: String?
    public var coupanType: String?
    public var tCoupanId: String?
    public var save: String?
    public var totalAmount: String?
    public var expiryDate: String?
    public var descriptionValue: String?
    public var saveAmount: String?
    public var currentAmount: String?
    public var imglink: String?
    public var dateAdded: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        coupan = json[SerializationKeys.coupan].string
        businessLogo = json[SerializationKeys.businessLogo].string
        coupanType = json[SerializationKeys.coupanType].string
        tCoupanId = json[SerializationKeys.tCoupanId].string
        save = json[SerializationKeys.save].string
        totalAmount = json[SerializationKeys.totalAmount].string
        expiryDate = json[SerializationKeys.expiryDate].string
        descriptionValue = json[SerializationKeys.descriptionValue].string
        saveAmount = json[SerializationKeys.saveAmount].string
        currentAmount = json[SerializationKeys.currentAmount].string
        imglink = json[SerializationKeys.imglink].string
        dateAdded = json[SerializationKeys.dateAdded].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = coupan { dictionary[SerializationKeys.coupan] = value }
        if let value = businessLogo { dictionary[SerializationKeys.businessLogo] = value }
        if let value = coupanType { dictionary[SerializationKeys.coupanType] = value }
        if let value = tCoupanId { dictionary[SerializationKeys.tCoupanId] = value }
        if let value = save { dictionary[SerializationKeys.save] = value }
        if let value = totalAmount { dictionary[SerializationKeys.totalAmount] = value }
        if let value = expiryDate { dictionary[SerializationKeys.expiryDate] = value }
        if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
        if let value = saveAmount { dictionary[SerializationKeys.saveAmount] = value }
        if let value = currentAmount { dictionary[SerializationKeys.currentAmount] = value }
        if let value = imglink { dictionary[SerializationKeys.imglink] = value }
        if let value = dateAdded { dictionary[SerializationKeys.dateAdded] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.coupan = aDecoder.decodeObject(forKey: SerializationKeys.coupan) as? String
        self.businessLogo = aDecoder.decodeObject(forKey: SerializationKeys.businessLogo) as? String
        self.coupanType = aDecoder.decodeObject(forKey: SerializationKeys.coupanType) as? String
        self.tCoupanId = aDecoder.decodeObject(forKey: SerializationKeys.tCoupanId) as? String
        self.save = aDecoder.decodeObject(forKey: SerializationKeys.save) as? String
        self.totalAmount = aDecoder.decodeObject(forKey: SerializationKeys.totalAmount) as? String
        self.expiryDate = aDecoder.decodeObject(forKey: SerializationKeys.expiryDate) as? String
        self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
        self.saveAmount = aDecoder.decodeObject(forKey: SerializationKeys.saveAmount) as? String
        self.currentAmount = aDecoder.decodeObject(forKey: SerializationKeys.currentAmount) as? String
        self.imglink = aDecoder.decodeObject(forKey: SerializationKeys.imglink) as? String
        self.dateAdded = aDecoder.decodeObject(forKey: SerializationKeys.dateAdded) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(coupan, forKey: SerializationKeys.coupan)
        aCoder.encode(businessLogo, forKey: SerializationKeys.businessLogo)
        aCoder.encode(coupanType, forKey: SerializationKeys.coupanType)
        aCoder.encode(tCoupanId, forKey: SerializationKeys.tCoupanId)
        aCoder.encode(save, forKey: SerializationKeys.save)
        aCoder.encode(totalAmount, forKey: SerializationKeys.totalAmount)
        aCoder.encode(expiryDate, forKey: SerializationKeys.expiryDate)
        aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
        aCoder.encode(saveAmount, forKey: SerializationKeys.saveAmount)
        aCoder.encode(currentAmount, forKey: SerializationKeys.currentAmount)
        aCoder.encode(imglink, forKey: SerializationKeys.imglink)
        aCoder.encode(dateAdded, forKey: SerializationKeys.dateAdded)
    }
    
}

