//
//  UserObject.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class UserObject: NSObject, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let latitude = "latitude"
        static let name = "name"
        static let isDelete = "isDelete"
        static let email = "email"
        static let dateAdded = "date_added"
        static let password = "password"
        static let address = "address"
        static let tUserId = "t_user_id"
        static let longitude = "longitude"
        static let zipcode = "zipcode"
        static let dob = "dob"
    }
    
    // MARK: Properties
    public var latitude: String?
    public var name: String?
    public var isDelete: String?
    public var email: String?
    public var dateAdded: String?
    public var password: String?
    public var address: String?
    public var userId: String?
    public var longitude: String?
    public var zipcode: String?
    public var dob: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        latitude = json[SerializationKeys.latitude].string
        name = json[SerializationKeys.name].string
        isDelete = json[SerializationKeys.isDelete].string
        email = json[SerializationKeys.email].string
        dateAdded = json[SerializationKeys.dateAdded].string
        password = json[SerializationKeys.password].string
        address = json[SerializationKeys.address].string
        userId = json[SerializationKeys.tUserId].string
        longitude = json[SerializationKeys.longitude].string
        zipcode = json[SerializationKeys.zipcode].string
        dob = json[SerializationKeys.dob].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = latitude { dictionary[SerializationKeys.latitude] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = isDelete { dictionary[SerializationKeys.isDelete] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = dateAdded { dictionary[SerializationKeys.dateAdded] = value }
        if let value = password { dictionary[SerializationKeys.password] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = userId { dictionary[SerializationKeys.tUserId] = value }
        if let value = longitude { dictionary[SerializationKeys.longitude] = value }
        if let value = zipcode { dictionary[SerializationKeys.zipcode] = value }
        if let value = dob { dictionary[SerializationKeys.dob] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.latitude = aDecoder.decodeObject(forKey: SerializationKeys.latitude) as? String
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.isDelete = aDecoder.decodeObject(forKey: SerializationKeys.isDelete) as? String
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.dateAdded = aDecoder.decodeObject(forKey: SerializationKeys.dateAdded) as? String
        self.password = aDecoder.decodeObject(forKey: SerializationKeys.password) as? String
        self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.tUserId) as? String
        self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? String
        self.zipcode = aDecoder.decodeObject(forKey: SerializationKeys.zipcode) as? String
        self.dob = aDecoder.decodeObject(forKey: SerializationKeys.dob) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(latitude, forKey: SerializationKeys.latitude)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(isDelete, forKey: SerializationKeys.isDelete)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(dateAdded, forKey: SerializationKeys.dateAdded)
        aCoder.encode(password, forKey: SerializationKeys.password)
        aCoder.encode(address, forKey: SerializationKeys.address)
        aCoder.encode(userId, forKey: SerializationKeys.tUserId)
        aCoder.encode(longitude, forKey: SerializationKeys.longitude)
        aCoder.encode(zipcode, forKey: SerializationKeys.zipcode)
        aCoder.encode(dob, forKey: SerializationKeys.dob)
    }
    
}


