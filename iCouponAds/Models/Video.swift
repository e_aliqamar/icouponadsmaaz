//
//  Video.swift
//
//  Created by Ali Qamar on 05/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Video: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let logoImage = "logoImage"
        static let tBusinessId = "t_business_id"
        static let businessVideo = "businessVideo"
        static let businessName = "businessName"
    }
    
    // MARK: Properties
    public var logoImage: String?
    public var tBusinessId: String?
    public var businessVideo: String?
    public var businessName: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        logoImage = json[SerializationKeys.logoImage].string
        tBusinessId = json[SerializationKeys.tBusinessId].string
        businessVideo = json[SerializationKeys.businessVideo].string
        businessName = json[SerializationKeys.businessName].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = logoImage { dictionary[SerializationKeys.logoImage] = value }
        if let value = tBusinessId { dictionary[SerializationKeys.tBusinessId] = value }
        if let value = businessVideo { dictionary[SerializationKeys.businessVideo] = value }
        if let value = businessName { dictionary[SerializationKeys.businessName] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.logoImage = aDecoder.decodeObject(forKey: SerializationKeys.logoImage) as? String
        self.tBusinessId = aDecoder.decodeObject(forKey: SerializationKeys.tBusinessId) as? String
        self.businessVideo = aDecoder.decodeObject(forKey: SerializationKeys.businessVideo) as? String
        self.businessName = aDecoder.decodeObject(forKey: SerializationKeys.businessName) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(logoImage, forKey: SerializationKeys.logoImage)
        aCoder.encode(tBusinessId, forKey: SerializationKeys.tBusinessId)
        aCoder.encode(businessVideo, forKey: SerializationKeys.businessVideo)
        aCoder.encode(businessName, forKey: SerializationKeys.businessName)
    }
    
}

