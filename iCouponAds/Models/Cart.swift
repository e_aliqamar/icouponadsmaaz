//
//  Cart.swift
//
//  Created by Ali Qamar on 16/07/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Cart: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let coupanType = "coupanType"
    static let itemName = "itemName"
    static let expiryDate = "expiryDate"
    static let descriptionValue = "description"
    static let type = "type"
    static let isExpired = "isExpired"
    static let businessLogo = "businessLogo"
    static let cartId = "cartId"
    static let id = "itemId"
    static let save = "save"
    static let totalAmount = "totalAmount"
    static let itemImage = "itemImage"
    static let saveAmount = "saveAmount"
    static let currentAmount = "currentAmount"
    static let dateTime = "DateTime"
  }

  // MARK: Properties
  public var coupanType: String?
  public var itemName: String?
  public var expiryDate: String?
  public var descriptionValue: String?
  public var type: String?
  public var isExpired: String?
  public var businessLogo: String?
  public var cartId: String?
  public var id: String?
  public var save: String?
  public var totalAmount: String?
  public var itemImage: String?
  public var saveAmount: String?
  public var currentAmount: String?
  public var dateTime: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: JSON) {
    self.init(json: object)
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    coupanType = json[SerializationKeys.coupanType].string
    itemName = json[SerializationKeys.itemName].string
    expiryDate = json[SerializationKeys.expiryDate].string
    descriptionValue = json[SerializationKeys.descriptionValue].string
    type = json[SerializationKeys.type].string
    isExpired = json[SerializationKeys.isExpired].string
    businessLogo = json[SerializationKeys.businessLogo].string
    cartId = json[SerializationKeys.cartId].string
    id = json[SerializationKeys.id].string
    save = json[SerializationKeys.save].string
    totalAmount = json[SerializationKeys.totalAmount].string
    itemImage = json[SerializationKeys.itemImage].string
    saveAmount = json[SerializationKeys.saveAmount].string
    currentAmount = json[SerializationKeys.currentAmount].string
    dateTime = json[SerializationKeys.dateTime].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = coupanType { dictionary[SerializationKeys.coupanType] = value }
    if let value = itemName { dictionary[SerializationKeys.itemName] = value }
    if let value = expiryDate { dictionary[SerializationKeys.expiryDate] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = isExpired { dictionary[SerializationKeys.isExpired] = value }
    if let value = businessLogo { dictionary[SerializationKeys.businessLogo] = value }
    if let value = cartId { dictionary[SerializationKeys.cartId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = save { dictionary[SerializationKeys.save] = value }
    if let value = totalAmount { dictionary[SerializationKeys.totalAmount] = value }
    if let value = itemImage { dictionary[SerializationKeys.itemImage] = value }
    if let value = saveAmount { dictionary[SerializationKeys.saveAmount] = value }
    if let value = currentAmount { dictionary[SerializationKeys.currentAmount] = value }
    if let value = dateTime { dictionary[SerializationKeys.dateTime] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.coupanType = aDecoder.decodeObject(forKey: SerializationKeys.coupanType) as? String
    self.itemName = aDecoder.decodeObject(forKey: SerializationKeys.itemName) as? String
    self.expiryDate = aDecoder.decodeObject(forKey: SerializationKeys.expiryDate) as? String
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
    self.isExpired = aDecoder.decodeObject(forKey: SerializationKeys.isExpired) as? String
    self.businessLogo = aDecoder.decodeObject(forKey: SerializationKeys.businessLogo) as? String
    self.cartId = aDecoder.decodeObject(forKey: SerializationKeys.cartId) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.save = aDecoder.decodeObject(forKey: SerializationKeys.save) as? String
    self.totalAmount = aDecoder.decodeObject(forKey: SerializationKeys.totalAmount) as? String
    self.itemImage = aDecoder.decodeObject(forKey: SerializationKeys.itemImage) as? String
    self.saveAmount = aDecoder.decodeObject(forKey: SerializationKeys.saveAmount) as? String
    self.currentAmount = aDecoder.decodeObject(forKey: SerializationKeys.currentAmount) as? String
    self.dateTime = aDecoder.decodeObject(forKey: SerializationKeys.dateTime) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(coupanType, forKey: SerializationKeys.coupanType)
    aCoder.encode(itemName, forKey: SerializationKeys.itemName)
    aCoder.encode(expiryDate, forKey: SerializationKeys.expiryDate)
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(isExpired, forKey: SerializationKeys.isExpired)
    aCoder.encode(businessLogo, forKey: SerializationKeys.businessLogo)
    aCoder.encode(cartId, forKey: SerializationKeys.cartId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(save, forKey: SerializationKeys.save)
    aCoder.encode(totalAmount, forKey: SerializationKeys.totalAmount)
    aCoder.encode(itemImage, forKey: SerializationKeys.itemImage)
    aCoder.encode(saveAmount, forKey: SerializationKeys.saveAmount)
    aCoder.encode(currentAmount, forKey: SerializationKeys.currentAmount)
    aCoder.encode(dateTime, forKey: SerializationKeys.dateTime)
  }

}
