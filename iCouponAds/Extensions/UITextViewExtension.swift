//
//  UITextViewExtension.swift
//  iCouponAds
//
//  Created by Ali Qamar on 03/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import UIKit

/// Extend UITextView and implemented UITextViewDelegate to listen for changes
extension UITextView: UITextViewDelegate {
    
//    /// Resize the placeholder when the UITextView bounds change
//    override open var bounds: CGRect {
//        didSet {
//            self.resizePlaceholder()
//        }
//    }
    
    //    @
    //Spacing Top ** UITextView **
    @IBInspectable var TopTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.top
        }
        set {
            
            self.textContainerInset = UIEdgeInsetsMake(newValue, self.textContainerInset.left, self.textContainerInset.bottom, self.textContainerInset.right)
            //textContainer.lineFragmentPadding = 0
        }
    }
    
    //Spacing Left ** UITextView **
    @IBInspectable var LeftTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.left
        }
        set {
            
            self.textContainerInset = UIEdgeInsetsMake(self.textContainerInset.top, newValue, self.textContainerInset.bottom, self.textContainerInset.right)
            //textContainer.lineFragmentPadding = 0
        }
    }
    
    //Spacing Bottom ** UITextView **
    @IBInspectable var BottomTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.bottom
        }
        set {
            
            self.textContainerInset = UIEdgeInsetsMake(self.textContainerInset.top, self.textContainerInset.left, newValue, self.textContainerInset.right)
            //textContainer.lineFragmentPadding = 0
        }
    }
    
    //Spacing Right ** UITextView **
    @IBInspectable var RightTextSpaceUITextView: CGFloat {
        get {
            return self.textContainerInset.right
        }
        set {
            
            self.textContainerInset = UIEdgeInsetsMake(self.textContainerInset.top, self.textContainerInset.left, self.textContainerInset.bottom, newValue)
            //textContainer.lineFragmentPadding = 0
        }
    }
    
//    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
//    ///
//    /// - Parameter textView: The UITextView that got updated
//    public func textViewDidChange(_ textView: UITextView) {
//        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
//            placeholderLabel.isHidden = self.text.characters.count > 0
//        }
//    }
//    
//    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
//    private func resizePlaceholder() {
//        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
//            let labelX = self.textContainer.lineFragmentPadding
//            let labelY = self.textContainerInset.top - 2
//            let labelWidth = self.frame.width - (labelX * 2)
//            let labelHeight = placeholderLabel.frame.height
//            
//            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
//        }
//    }
//    
//    /// Adds a placeholder UILabel to this UITextView
//    private func addPlaceholder(_ placeholderText: String) {
//        let placeholderLabel = UILabel()
//        
//        placeholderLabel.text = placeholderText
//        placeholderLabel.sizeToFit()
//        
//        placeholderLabel.font = self.font
//        placeholderLabel.textColor = UIColor.lightGray
//        placeholderLabel.tag = 100
//        
//        placeholderLabel.isHidden = self.text.characters.count > 0
//        
//        self.addSubview(placeholderLabel)
//        self.resizePlaceholder()
//        self.delegate = self
//    }
}
