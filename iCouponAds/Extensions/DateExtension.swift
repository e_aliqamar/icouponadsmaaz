//
//  DateExtension.swift
//  iCouponAds
//
//  Created by Ali Qamar on 04/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation

extension Date {
    func daysLeft() -> String {
        let now = Date()
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: now)
        let date2 = calendar.startOfDay(for: self)
        
        let components = calendar.dateComponents([.day, .month, .year, .hour, .minute, .second], from: date1, to: date2)
        
        if components.year! > 0 {
            if components.year! > 1 {
                return "\(String(describing: components.year!)) Years"
            }
            return "\(String(describing: components.year!)) Year"
        } else if components.month! > 0 {
            if components.month! > 1 {
                return "\(String(describing: components.month!)) Months"
            }
            return "\(String(describing: components.month!)) Month"
        } else if components.day! > 0 {
            if components.day! > 1 {
                return "\(String(describing: components.day!)) Days"
            }
            return "\(String(describing: components.day!)) Day"
        } else if components.hour! > 0 {
            if components.hour! > 1 {
                return "\(String(describing: components.hour!)) Hours"
            }
            return "\(String(describing: components.hour!)) Hour"
        } else if components.minute! > 0 {
            if components.minute! > 1 {
                return "\(String(describing: components.minute!)) Minutes"
            }
            return "\(String(describing: components.minute!)) Minute"
        } else if components.second! > 0 {
            if components.second! > 1 {
                return "\(String(describing: components.second!)) Seconds"
            }
            return "\(String(describing: components.second!)) Second"
        }
        return "0 day"
    }
}
