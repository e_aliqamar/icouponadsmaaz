//
//  UITextFieldExtension.swift
//  iCouponAds
//
//  Created by Ali Qamar on 03/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import UIKit
import AMXFontAutoScale

extension UITextField
{
    enum Direction
    {
        case Left
        case Right
    }
    
    func AddImage(direction: Direction, imageName: String, Frame: CGRect, backgroundColor: UIColor)
    {
        let View = UIView(frame: Frame)
        View.backgroundColor = backgroundColor
        
        let imageView = UIImageView(frame: Frame)
        imageView.contentMode = .center
        imageView.image = UIImage(named: imageName)
        
        View.addSubview(imageView)
        
        if Direction.Left == direction
        {
            self.leftViewMode = .always
            self.leftView = View
        }
        else
        {
            self.rightViewMode = .always
            self.rightView = View
        }
    }
}

extension UITextField: AMXFontAutoScalable {
    public var amx_originalFontPointSize: CGFloat {
        get {
            return self.amx_originalFontPointSize
        }
        set(amx_originalFontPointSize) {
            
        }
    }
    
    public var amx_fontSizeUpdateHandler: AMXFontUpdateHandler! {
        get {
            return nil
        }
        set(amx_fontSizeUpdateHandler) {
            
        }
    }
    
    public static var amx_autoScaleEnabled: Bool {
        get {
            return true
        }
        set {
            //self.amx_autoScaleEnabled = newValue
        }
    }
    
    public var amx_autoScaleEnabled: Bool {
        get {
            return true
        }
        set {
            //self.amx_autoScaleEnabled = newValue
        }
    }
    
    
    public var amx_referenceScreenSize: AMXScreenSize {
        get {
            return self.amx_referenceScreenSize
        }
        set {
            //self.amx_referenceScreenSize = newValue
        }
    }
    
    public static var amx_referenceScreenSize: AMXScreenSize {
        get {
            return self.amx_referenceScreenSize
        }
        set {
            //self.amx_referenceScreenSize = newValue
        }
    }
    
    public static func amx_autoScaleFont(forReferenceScreenSize screenSize: AMXScreenSize) {
        self.amx_referenceScreenSize = screenSize
    }
    
    public func amx_autoScaleFont(forReferenceScreenSize screenSize: AMXScreenSize) {
        self.amx_referenceScreenSize = screenSize
    }
}
