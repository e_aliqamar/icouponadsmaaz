//
//  UIViewControllerExtension.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func setTitleLogo() {
        let imageView = UIImageView(image: TITLE_LOGO)
        self.navigationItem.titleView = imageView
    }
    
    func presentNib(nib: UIView, with visualEffect: UIVisualEffectView, in view: UIView) -> UIVisualEffectView {
        
        visualEffect.frame = self.view.frame
        self.view.addSubview(visualEffect)
        visualEffect.bringSubview(toFront: self.view)
        
        nib.frame = CGRect(x: 0, y: 0, width: visualEffect.frame.size.width - 40, height: WINDOW_HIEGHT / 3)
        nib.center = view.center
        self.view.addSubview(nib)
        nib.bringSubview(toFront: self.view)
        
        return visualEffect
    }
    
    func presentNib(nib: UIView) {
        
        nib.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 40, height: WINDOW_HIEGHT / 3)
        nib.center = self.view.center
        self.view.addSubview(nib)
        nib.bringSubview(toFront: self.view)
    }
    
    func presentNib(nib: UIView, with frame: CGRect) {
        
        nib.frame = frame
        self.view.addSubview(nib)
        nib.bringSubview(toFront: self.view)
    }
    
    func presentNib(nib: UIView, with frame: CGRect, in view: UIView) {
        
        nib.frame = frame
        nib.center = view.center
        self.view.addSubview(nib)
        nib.bringSubview(toFront: self.view)
    }
}
