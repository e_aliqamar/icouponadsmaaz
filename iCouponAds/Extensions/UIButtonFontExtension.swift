//
//  UIButtonFontExtension.swift
//  iCouponAds
//
//  Created by Ali Qamar on 03/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import AMXFontAutoScale

extension UIButton: AMXFontAutoScalable {
    public var amx_originalFontPointSize: CGFloat {
        get {
            return self.amx_originalFontPointSize
        }
        set(amx_originalFontPointSize) {
            
        }
    }
    
    public var amx_fontSizeUpdateHandler: AMXFontUpdateHandler! {
        get {
            return nil
        }
        set(amx_fontSizeUpdateHandler) {
            
        }
    }
    
    public static var amx_autoScaleEnabled: Bool {
        get {
            return true
        }
        set {
            //self.amx_autoScaleEnabled = newValue
        }
    }
    
    public var amx_autoScaleEnabled: Bool {
        get {
            return true
        }
        set {
            //self.amx_autoScaleEnabled = newValue
        }
    }
    
    public var amx_referenceScreenSize: AMXScreenSize {
        get {
            return self.amx_referenceScreenSize
        }
        set {
            //self.amx_referenceScreenSize = newValue
        }
    }
    
    public static var amx_referenceScreenSize: AMXScreenSize {
        get {
            return self.amx_referenceScreenSize
        }
        set {
            //self.amx_referenceScreenSize = newValue
        }
    }
    
    public static func amx_autoScaleFont(forReferenceScreenSize screenSize: AMXScreenSize) {
        self.amx_referenceScreenSize = screenSize
    }
    
    public func amx_autoScaleFont(forReferenceScreenSize screenSize: AMXScreenSize) {
        self.amx_referenceScreenSize = screenSize
    }
}
