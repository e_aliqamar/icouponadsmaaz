//
//  StringExtension.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation

extension String {
    
    func isEmptyOrWhitespace() -> Bool {
        if(self.isEmpty) {
            return true
        }
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces) == ""
    }
    
    func toLocalDate(date: String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd HH:mm:ss"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone?
            return result
        }()
        
        let serverTime = date
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "hh:mm a"
            return result
        }()
        
        return localDateFormatter.string(from: localTime)
    }
    
    func toDate() -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone?
        let date = dateFormatter.date(from: self)!
        return date
    }
    
}
