//
//  UIImageViewExtension.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    
    func setImageFromUrl(urlStr:String) {
        
        let url = URL(string: urlStr)!
        print(url)
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url,
                         placeholder: nil,
                         options: [.transition(.fade(1))],
                         progressBlock: nil,
                         completionHandler: nil)
    }
}
