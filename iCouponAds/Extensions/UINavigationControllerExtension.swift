//
//  UINavigationControllerExtension.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    func resetNavigationBar() {
        self.navigationBar.isHidden = false
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
    }
    
    func setNavigationBarColor(color: UIColor) {
        self.navigationBar.barTintColor = color
    }
    
    func transparentNavigationBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.view.backgroundColor = .clear
    }
    
    func setNavigationBarTitleColor(color: UIColor) {
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: color]
    }
    
    func hideNavigationBar(isHidden: Bool? = true) {
        self.navigationBar.isHidden = isHidden!
    }
}

extension UINavigationItem {
    
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.textColor = .white
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17)
        one.sizeToFit()
        
        
        let two = UILabel()
        two.textColor = .white
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 12)
        two.textAlignment = .center
        two.sizeToFit()
        
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        stackView.alignment = .center
        
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        
        one.sizeToFit()
        two.sizeToFit()
        
        
        self.titleView = stackView
    }
}
