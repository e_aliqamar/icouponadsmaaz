//
//  AppDelegate.swift
//  iCouponAds
//
//  Created by Ali Qamar on 29/06/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AMXFontAutoScale
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        
//        UILabel.amx_autoScaleFont(forReferenceScreenSize: .size4p7Inch)
        UIButton.amx_autoScaleFont(forReferenceScreenSize: .size4p7Inch)
        UITextField.amx_autoScaleFont(forReferenceScreenSize: .size4p7Inch)
        
        GMSServices.provideAPIKey("AIzaSyCQoHsYRIV6EKGvnH0Hbh_L9SU7utne7IU")
        
        self.navigateTOInitialViewController()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func navigateTOInitialViewController() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let loginStatus = UserManager.isUserLogin()
        
        if loginStatus == true
        {
            let vc = RootViewController.instantiateFromStoryboard()
            self.window?.rootViewController = vc
        }
        else {
            let vc = RootNavigationViewController.instantiateFromStoryboard()
            vc.viewControllers = [LoginViewController.instantiateFromStoryboard()]
            self.window?.rootViewController = vc
        }
    }

}

