//
//  CartTableViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 16/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    // MARK: - Properties
    var indexPath: IndexPath!
    var delegate: ButtonClickDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func removeAction(_ sender: UIButton) {
        self.delegate?.click!(indexPath: self.indexPath)
    }
    
}
