//
//  CategoriesTableViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 10/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheckmark: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - IBAction
    
    @IBAction func checkmarkAction(_ sender: UIButton) {
        // sender.isSelected = !sender.isSelected
    }
    
}
