//
//  ClippingTableViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 03/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class ClippingTableViewCell: UITableViewCell {
   
    // MARK: - IBOutlet
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties

    let cellIdentifier = "ClippingCollectionViewCell"
    
    var clipping: Clipping!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        self.collectionViewHeightConstraint.constant = 228 * 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
//MARK:-  UICollectionViewDelegate, UICollectionViewDataSource

extension ClippingTableViewCell: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ClippingCollectionViewCell
        
        cell.imgBanner.setImageFromUrl(urlStr: self.clipping.image!)
        
        if self.clipping.isExpired == "1" {
            cell.isViewExpired.isHidden = true
        } else {
            cell.isViewExpired.isHidden = false
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItems: CGFloat = 2.0
        let padding: CGFloat = 5.0

        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = collectionViewSize.width / numberOfItems - padding
        collectionViewSize.height = 228//collectionViewSize.height / 2.5
        return collectionViewSize
    }
}

