//
//  AddCategoryTableViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 12/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class AddCategoryTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var txtTitle: UITextField!
    
    // MARK: - Properties
    var delegate: ButtonClickDelegate?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
