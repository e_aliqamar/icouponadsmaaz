//
//  InStoreTableViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 05/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class InStoreTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var imgLogo: UIImageViewAligned!
    @IBOutlet weak var lblDealsCount: UILabel!
    @IBOutlet weak var lblUrl: UILabel!
    
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    
    // MARK: - Properties
    var delegate: ButtonClickDelegate?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgLogo.alignLeft = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func locationAction(_ sender: UIButton) {
        self.delegate?.locationClick!(indexPath: indexPath!)
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        self.delegate?.shareClick!(indexPath: indexPath!)
    }
    
    @IBAction func playAction(_ sender: UIButton) {
        self.delegate?.playClick!(indexPath: indexPath!)
    }
    
    @IBAction func detailAction(_ sender: UIButton) {
        self.delegate?.detailClick!(indexPath: indexPath!)
    }
    
}
