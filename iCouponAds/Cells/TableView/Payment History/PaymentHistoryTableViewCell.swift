//
//  PaymentHistoryTableViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 11/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class PaymentHistoryTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var viewColored: UIView!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
