//
//  CouponDetailTableViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 13/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class CouponDetailTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var txtShortDescription: UITextView!
    @IBOutlet weak var txtLongDescription: UITextView!
    @IBOutlet weak var btnAddToCart: UIButton!
    
    // MARK: - Properties
    var indexPath: IndexPath!
    var delegate: ButtonClickDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addToCartAction(_ sender: UIButton) {
        self.delegate?.click!(indexPath: indexPath)
    }
    
}
