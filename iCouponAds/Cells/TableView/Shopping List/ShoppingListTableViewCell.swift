//
//  ShoppingListTableViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 17/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class ShoppingListTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheckmark: UIButton!
    
    // MARK: - Properties
    var indexPath: IndexPath!
    var delegate: ButtonClickDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        self.delegate?.click!(indexPath: self.indexPath)
    }
    
}
