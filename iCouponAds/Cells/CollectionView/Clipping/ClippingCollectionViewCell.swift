//
//  ClippingCollectionViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 03/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class ClippingCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var isViewExpired: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
