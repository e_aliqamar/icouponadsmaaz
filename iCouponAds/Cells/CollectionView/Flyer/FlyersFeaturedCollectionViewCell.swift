//
//  FlyersFeaturedCollectionViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class FlyersFeaturedCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOutlet

    @IBOutlet weak var lblDaysLeft: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewIsNew: UIView!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
