//
//  CouponPercentOffCollectionViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 02/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class CouponPercentOffCollectionViewCell: UICollectionViewCell {

    
    // MARK: - IBOutlet
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblActualPrice: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
