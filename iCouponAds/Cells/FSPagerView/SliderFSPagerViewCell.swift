//
//  SliderFSPagerViewCell.swift
//  iCouponAds
//
//  Created by Ali Qamar on 03/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit
import FSPagerView

class SliderFSPagerViewCell: FSPagerViewCell {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var imageBanner: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
