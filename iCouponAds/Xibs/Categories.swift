//
//  Categories.swift
//  iCouponAds
//
//  Created by Ali Qamar on 10/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class Categories: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    // MARK: - IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var delegate: CategoriesDelegate?
    let cellIdentifier = "CategoriesTableViewCell"
    var categories: [Category] = [Category]()
    var checked: [Bool] = [Bool]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    class func instantiateFromNib() -> Categories? {
        return UINib(nibName: "Categories", bundle: nil).instantiate(withOwner: nil, options: nil).first as? Categories
    }
    
    func registerNib() {
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        self.tableView.tableFooterView = UIView()
        
        resetChecks()
    }
    
    func resetChecks() {
        for _ in 0..<self.categories.count {
            self.checked.append(false)
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func skipAction(_ sender: UIButton) {
        self.removeFromSuperview()
        self.delegate?.skip!()
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        
        var indexes: [IndexPath] = [IndexPath]()
        for (index, item) in self.checked.enumerated() {
            if item {
                indexes.append(IndexPath.init(row: index, section: 0))
            }
        }
        
        self.removeFromSuperview()
        self.delegate?.next!(indexes: indexes)
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension Categories: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CategoriesTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CategoriesTableViewCell
        
        let obj = self.categories[indexPath.row]
        cell.lblTitle.text = obj.category!
        
        if !checked[indexPath.row] {
            cell.btnCheckmark.isSelected = false
        } else if checked[indexPath.row] {
            cell.btnCheckmark.isSelected = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        
        if let cell = tableView.cellForRow(at: indexPath) as? CategoriesTableViewCell {
            
            if cell.btnCheckmark.isSelected {
                cell.btnCheckmark.isSelected = false
                checked[indexPath.row] = false
            } else {
                cell.btnCheckmark.isSelected = true
                checked[indexPath.row] = true
            }
        }
    }
}
