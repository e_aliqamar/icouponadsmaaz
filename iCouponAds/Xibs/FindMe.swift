//
//  FindMe.swift
//  iCouponAds
//
//  Created by Ali Qamar on 06/07/2018.
//  Copyright © 2018 Muhammad Maaz Ul Haq. All rights reserved.
//

import UIKit

class FindMe: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var txtZipCode: UITextField!
    
    // MARK: - Properties
    
    var delegate: FindMeDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    class func instantiateFromNib() -> FindMe? {
        return UINib(nibName: "FindMe", bundle: nil).instantiate(withOwner: nil, options: nil).first as? FindMe
    }
    
    // MARK: - IBAction
    
    @IBAction func nextAction(_ sender: UIButton) {
        guard let zipCode = txtZipCode.text, AppHelper.isValid(zipCode: zipCode)  else {
            Alert.showAlert(title: "Error", message: "Zip Code must be 5 to 11 characters long")
            return
        }
        self.removeFromSuperview()
        self.delegate?.findMe!(zipCode: zipCode)
    }
}

//MARK:-  UITextFieldDelegate

extension FindMe: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
}

